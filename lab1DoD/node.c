#include "node.h"


void printInvalidParams(char *progName) {
    fprintf(stderr,
        "%s\n%s %s\n",
        "Invalid parameters",
        progName,
        "{tcp|udp} <LOCAL PORT> <NEXT HOST> <NEXT PORT>");
}

void argumentCheck(int argc, char ** argv){
    if (argc != 5) {
        printInvalidParams(argv[0]);
        exit(EXIT_FAILURE);
    }

    if(strstr(argv[1], "tcp") == NULL
    && strstr(argv[1], "udp") == NULL) {
        printInvalidParams(argv[0]);
        exit(EXIT_FAILURE);
    }
}


int* getIntFromStr(char *givenStr) {
    int p;
    int *fixedInt;
    char *strRest;
    if ((p = strtol(givenStr, &strRest, 10)) == 0) {
        return NULL;
    }
    fixedInt = malloc(sizeof(int)*strlen(givenStr));
    *fixedInt = p;
    return fixedInt;
}

char* getCurrentHostName() {
    char tmpHost[255];
    memset(tmpHost, 0, 255);
    if (gethostname(tmpHost, 255) != 0) {
        return 0;
    }
    char* host = calloc(strlen(tmpHost)+1, sizeof(char));
    strcpy(host, tmpHost);
    return host;
}

host_info* generateHostInfo(char* nodeType, char* name, char* port){
    host_info* host = malloc(sizeof(host_info));
    host->protocolType = (strstr(nodeType, "udp") != NULL) ? UDP : TCP;
    host->name = name;
    host->port = getIntFromStr(port);

    return host;
}

arguments* createArgs (char** argv) {
    arguments* args = malloc(sizeof(arguments));
    list* messages = list_empty();
    setMemHandler(messages, free);
    args->messages = messages;
    host_info* serverHost = generateHostInfo(argv[1], getCurrentHostName(), argv[2]);
    args->server = serverHost;
    host_info* clientHost = generateHostInfo(argv[1], strdup (argv[3]), argv[4]);
    args->client = clientHost;

    return args;
}


/*
 * Starts and joins the sender and receiver threads.
 */
int main(int argc, char **argv) {
    argumentCheck(argc, argv);
    arguments* args = createArgs(argv);

    struct sigaction* handler = setSignalHandler(&SIG_handler);

    phase = 0;
    is_nodeOnline = false;

    // Create receiver thread
    pthread_t listenerThread;
    if (pthread_create(&listenerThread, NULL, &receiver_init, args) < 0) {
        perror("Error creating listener-thread");
        return EXIT_FAILURE;
    }

    // create sender thread
    pthread_t senderThread;
    if (pthread_create(&senderThread, NULL, &sender_init, args) < 0) {
        perror("Error creating listener-thread");
        return EXIT_FAILURE;
    }

    pthread_join(senderThread, NULL);
    pthread_join(listenerThread, NULL);


    freeArguments(args);
    free(handler);
    return EXIT_SUCCESS;

}
