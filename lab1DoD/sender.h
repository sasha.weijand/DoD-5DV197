/*
 *
 */
#include "shared.h"

#ifndef _SENDER_H
#define _SENDER_H

/*
 * Takes a message from the queue of messages
 * and writes it to the socket (called sfd).
 */
void sendMessageFromQueue(list* messages, int sfd);

/*
 * Creates a new message and queues it.
 */
void startElection(list* messages, char* ownID);


/*
 * If no message has been received and we're in phase 1, startElection will be called.
 * Then sendMessageFromQueue will be called.
 */
void sendMessages (list* messages, char* ownID, int sfd);

/*
 * Initiates the sender thread and then calls sendMessages.
 */
void* sender_init(void* arg);

#endif
