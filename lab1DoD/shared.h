#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE 700
#endif
#ifndef _POSIX_SOURCE
#define _POSIX_SOURCE
#endif

#define BUFFERSIZE 100
#define UDP 0
#define TCP 1
#define RECEIVER 2
#define SENDER 3
#define ELECTION "ELECTION"
#define ELECTION_OVER "ELECTION_OVER"
#define MESSAGE "MESSAGE"

#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netdb.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <math.h>
#include <sys/time.h>

#include "thread_list_lib.h"
#include "signal_handler.h"

#ifndef _SHARED_H
#define _SHARED_H

bool is_nodeOnline;
int phase;

typedef struct host_info {
    char* name;
    int* port;
    int protocolType;
}host_info;

typedef struct arguments {
    list* messages;
    host_info* server;
    host_info* client;
}arguments;



/*  Sets the addressfam to ipv4 and sockettype according to given host
 *  @return:
 */
void setHints(struct addrinfo* hints, host_info* host);

/*  Returns the host port as a string
 *  @return: string containing portnumber
 */
char* getHostPort(host_info* host);

/*  Generates an addrinfo from given hostname, port and hints
 *  @return: result addrinfo
 */
struct addrinfo* generateAddressInfo(char* hostName, char* port, struct addrinfo* hints, struct addrinfo* result);

/*  Creates and initiates a socket according to given type:
 *   - If socket type is SENDER: socket is also connected
 *   - If socket type is RECEIVER: socket is also binded to local port
 *
 *  @return: sfd
 */
int initSocket (struct addrinfo* result, int socketType, host_info* host);


/*  Generates the nodeID by concatenating ',' and portnumber  with hostname
 *
 *  @return: complete nodeID
 */
char* generateNodeID( int port);

/*  Parses the given message and returns the nodeID
 *
 *  @return: nodeID
 */
char* parseNodeIDfromMessage(char* message);

/*  Parses the given message and returns the message type
 *
 *  @return: messagetype
 */
char* parseMessageTypeFromMessage(char* message);

/*  Creates a new message according to given type, ID and text
 *
 *  @return: new message
 */
char* createMessage (char* messageType, char* nodeId, char* text);

/*  Puts node hostname to given string
 *
 *  @return: 0 on success else > 0
 */
int getFQDN(char *fqdn, size_t n);

/*  Frees the given argument structure
 *
 *  @return:
 */
void freeArguments (arguments* args);

/*  Frees the given host_info structure
 *
 *  @return: string of nodeID
 */
void freeHostInfo (host_info* host);

#endif
