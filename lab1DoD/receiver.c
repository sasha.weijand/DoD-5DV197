/*
*
*/
#include "receiver.h"


/* Sets up a socket for receiving messages for the program
* @param arg   host of the program.
* @returned    void pointer (needed to end the thread)
*/
void *receiver_init(void * arg) {
    arguments* args = (arguments*) arg;
    host_info *host = (host_info*) args->server;
    list* messages = (list*) args->messages;

    struct addrinfo hints, *result;
    setHints(&hints, host);

    char* listeningPort = getHostPort(host);

    // Get addrinfo for the receiving part
    result = generateAddressInfo(NULL, listeningPort, &hints, result);
    // Find correct adress and try to bind a socket
    int sfd = initSocket(result, RECEIVER, host);
    // Choose listener.
    receiver_listen(host, sfd, messages);
    // listen-function has stopped, close and free
    freeaddrinfo(result);
    close(sfd);
    free(listeningPort);
    return NULL;
}

int tcpAccept(int sfd){
    struct sockaddr_in clientAddress;
    memset(&clientAddress, 0, sizeof(struct sockaddr_in));
    int addressLength = sizeof(struct sockaddr_in);

    listen(sfd, 1);
    int clientSocket = accept(sfd,(struct sockaddr *)&clientAddress,
        (socklen_t*)&addressLength);
    if(clientSocket < 0){
        perror("Receiver: Accept failed");
    }
    return clientSocket;
}

char* readNBytes(int nrOfBytes, int protocolType, int sfd) {
    struct sockaddr_storage clientAddress;
    socklen_t clientAddressLen;
    char *buffer = calloc(1, nrOfBytes);
    ssize_t nread;

    if(protocolType == TCP)
        nread = recv(sfd, buffer, nrOfBytes, 0);
    else if(protocolType == UDP){
        clientAddressLen = sizeof(struct sockaddr_storage);
        nread = recvfrom(sfd, buffer, nrOfBytes, 0,
            (struct sockaddr *) &clientAddress, &clientAddressLen);
    }

    if (nread != nrOfBytes) {
        if(nread == -1)
            fprintf(stderr, "Connection Timed out: Closing program\n");
        else
            fprintf(stderr, "Must read 100 bytes: Wrong amount of bytes read: %ld\n", nread);
        free(buffer);
        return NULL;
    }
    return buffer;
}

void electionProcedure(list* messages, char* receivedMessage, char* messageType
    , char* ownID, char* msgID, struct timeval* startTime){

    char* newMessage = NULL;

    if(!is_nodeOnline)
        is_nodeOnline = true;

    if(strcmp(messageType, ELECTION) == 0){
        if(strcmp(ownID, msgID) == 0)
            newMessage = createMessage(ELECTION_OVER, ownID, NULL);
        else if(strcmp(ownID, msgID) > 0)
            newMessage = createMessage(ELECTION, ownID, NULL);

    }else if(strcmp(messageType, ELECTION_OVER)== 0){
        phase = 2;
        list_clear_t(messages);
        if(strcmp(ownID, msgID) == 0){
            newMessage = createMessage(MESSAGE, ownID, "PANG PÅ PUNGEN I PORTUGAL\n");
            gettimeofday(startTime, NULL);
        }
    }

    if(newMessage == NULL){
        list_queue_t(messages, receivedMessage);
    }else {
        list_queue_t(messages, newMessage);
        free(receivedMessage);
    }

}

void messageRingProcedure(list* messages, char* receivedMessage, char* messageType
    , char* ownID, char* msgID, struct timeval* startTime, struct timeval* stopTime, int* laps){

    list_clear_t(messages);
    if(strcmp(messageType, MESSAGE) == 0){
        if(strcmp(ownID, msgID) == 0){
            if(*laps < 10000){
                list_queue_t(messages, receivedMessage);
                (*laps)++;
            }else{
                gettimeofday(stopTime, NULL);
                double totTime = stopTime->tv_usec - startTime->tv_usec;
                printf("AVARAGE TIME FOR 10000 LAPS: %f sec\n", totTime/(1000000 * *laps));
                shouldExit = true;
            }
        }else{
            list_queue_t(messages, receivedMessage);
        }
    }
}

void receiver_listen(host_info *host, int sfd, list* messages){
    int clientSocket;
    char* ownID = generateNodeID(*(host->port));
    struct timeval startTime;
    struct timeval stopTime;
    char* msg;

    int laps = 0;

    if(host->protocolType == TCP)
        clientSocket = tcpAccept(sfd);


    while(!shouldExit){
        if(host->protocolType == TCP)
            msg = readNBytes(BUFFERSIZE, TCP, clientSocket);
        else
            msg = readNBytes(BUFFERSIZE, UDP, sfd);

        if(msg == NULL){
            shouldExit = true;
            continue;
        }

        char* messageType = parseMessageTypeFromMessage(msg);
        char* msgID = parseNodeIDfromMessage(msg);

        if(phase == 1)
            electionProcedure(messages, msg, messageType, ownID, msgID, &startTime);
        else if(phase == 2)
            messageRingProcedure(messages, msg, messageType, ownID, msgID, &startTime, &stopTime, &laps);


        free(messageType);
    }
    free(msg);
    free(ownID);
    if(host->protocolType == TCP) {
        shutdown(clientSocket, SHUT_RDWR);
        close(clientSocket);
    }
    exit(0);
}
