#include "list_lib.h"
/** A double linked list
 **/



/** Checks if previous memoryAllocation succeded or returned NULL
  * in: pointer to memory allocated
  * out:
 **/
void check_memoryAllocation(data d){
	if(d == NULL){
		fprintf(stderr,"Failed to allocate memory!\n");
		exit(EXIT_FAILURE);
	}
}

/** Creates a new list and returns a pointer to it
  * in:
  * out: pointer to the created list
 **/
list * list_empty (void){
	list *l = malloc(sizeof(list));
	check_memoryAllocation(l);
	l->top = calloc(1, sizeof(list_node));
	check_memoryAllocation(l->top);
	l->bot = calloc(1, sizeof(list_node));
	check_memoryAllocation(l->bot);
	l->top->next = l->bot;
	l->bot->prev = l->top;
	l->size=0;
	l->freeFunc = NULL;
	init_mutex_lock(l);

	return l;
}


/** Sets the memHandlerFunction to the given list
  * in: pointer to list
  * out:
 **/
void setMemHandler(list* l, memFreeFunc* f) {
	l->freeFunc = f;
}


/** Sets the compareFunction to the given list
  *in: pointer to list
  *out:
 **/
void setCompareFunc(list* l, compareFunc* f){
	l->compFunc = f;
}

/** Returns the size of the given list
  * in: pointer to list
  * out: sie of the given list
 **/
int list_size(list* l){
	int size = l->size;
	return 	size;
}


/** Returns the first position of the list
  * in: pointer to list
  * out: the first position in the list
 **/
list_position list_first (list* l){
	list_position p = l->top->next;
	return p;
}


/** Returns the last position in the list
  * in: pointer to the list
  * out: the last position in the list
 **/
list_position list_last (list *l){
	list_position p = l->bot;
	return p;
}


/** Takes position and returns the next position in the list
  * in: position
  * out: next position
 **/
list_position list_next (list* l, list_position p){
	list_position next_position = p->next;
	return next_position;
}


/** Takes a position and return the previous position in the list
  * in: position
  * out: previous position
 **/
list_position list_previous (list* l, list_position p){
	list_position previous_position = p->prev;
	return previous_position;
}


/** Takes an integer as index and and return the position under given index .
  * If given index is larger than the size of the list NULL is returned
  * in: pointer to list, index
  * out: position corresponding to give index
 **/
list_position list_get_pos_at (list*l, int index){

	if(index < l->size){
		list_position p;

		if((index <= l->size/2)){
			p = list_first(l);

			for(int i = 0; i<index; i++){
				p = list_next(l, p);
			}
		}else{
			p = list_last(l);

			for(int i = 1; i< (l->size-index); i++){
				p = list_previous(l, p);
			}
		}
		return p;
	}
	return NULL;
}


/** Takes a data element and inserts it at given position in the list and returns the new node
  * in: pointer to the list, data, position to insert at
  * out: newNode
 **/
list_position list_insert (list* l, data dp, list_position p){
	list_position newNode = malloc(sizeof(list_node));
	check_memoryAllocation(l);
	newNode->value = dp;
	newNode->next = p;
	newNode->prev = p->prev;
	p->prev = newNode;
	newNode->prev->next = newNode;

	l->size++;

	return newNode;
}

/** Takes  data element and inserts it in th elist at the position corresponding to the given index
  * If given index is larger than size of the list, NULL is returned
  * in: pointer to the list, data, index to insert at
  * out: newNode or NULL
 **/
list_position list_insert_at(list* l, data dp, int index){

	list_position p = list_get_pos_at (l, index);
	if(p)
		return list_insert(l, dp, p);
	else
		return NULL;
}


/** Returns true or false wether the given list is empty or not
  * in: pointer to given list
  * out: true or false
 **/
bool list_isEmpty (list *l){
	bool isEmpty;
	if(l->top->next == l->bot)
		 isEmpty = true;
	else
		 isEmpty = false;

	return isEmpty;
}


/** Returns the value corresponding to the given position
  * in: position
  * out: value
 **/
data list_inspect(list* l, list_position p) {
	data value = p->value;

	return value;
}


/** Returns the value corresponding to the given index
  * Return NULL if given index is larger than size of the list
  * in: pointer ot given list
  * out: value or NULL
 **/
data list_inspect_at(list* l, int index) {
	list_position p = list_get_pos_at(l, index);

	if(p)
		return list_inspect(l, p);
	return NULL;
}


/** Removes the given position in the list
  * if memHandler is set, the value is also freed
  * in: pointer to list, position
  * out: return the current position
 **/
list_position list_remove (list*l, list_position p){
	list_position ret = p->next;
	p->prev->next = p->next;
	p->next->prev = p->prev;

	if(p->value != NULL && l->freeFunc != NULL){
		l->freeFunc(p->value);
	}
	free(p);

	l->size--;

	return ret;
}

/** Removes the given position to the list corresponding to the given index
  * If given index is larger than size of the list, NULL is returned
  * in: pointer to list, index
  * out: current position or NULL
 **/
list_position list_remove_at (list* l, int index){
	list_position p = list_get_pos_at(l, index);

	if(p)
		return list_remove(l, p);
	else
		return NULL;
}


/** Takes two positions and swaps their values
  * in: position a and postion b
  * out:
 **/
void list_swapValues (list_position a, list_position b){
	data tmp = a->value;
	a->value = b->value;
	b->value = tmp;

}


/** Sorts the given list, using an insertion-sort algorithm, according to given sort terms
  * in: pointer to list
  * out:
 **/
void list_insertionSort(list *l){
	if(!list_isEmpty(l)){
		list_position current = list_next(l, list_first(l));
		list_position previous = list_previous(l, current);
		list_position mark;
		while(previous != list_last(l)){
			if(l->compFunc (list_inspect(l, current), list_inspect(l, previous))){
				mark = current;
				while(l->compFunc (list_inspect(l, current), list_inspect(l, previous)) && current != list_first(l)){
					list_swapValues(current, previous);
					current = previous;
					if(previous != list_first(l))
						previous = list_previous(l, current);
				}
				current = mark;
				previous = list_previous(l, current);
			}
			previous = current;
			if(current != list_last(l))
				current = list_next(l, current);
		}
	}
}

void list_push (list* l, data dp){
	list_insert(l, dp, list_first(l));
}

list_position list_pop(list* l){
	if(list_isEmpty(l))
        return NULL;
    return list_remove(l, list_first(l));
}

void list_queue (list* l, data dp){
	list_insert(l, dp, list_last(l));
}

list_position list_dequeue (list* l){
	if(list_isEmpty(l))
		return NULL;
	return list_remove(l, list_first(l));
}

void list_clear(list* l){
	while(!list_isEmpty)
		list_dequeue(l);
}

/** Frees all allocated memory to given list
  * in: pointer to list
  * out:
 **/
void list_free (list* l){

	while(!list_isEmpty(l)){
		list_remove(l, list_first(l));
	}

	free(l->top);
	free(l->bot);
	destroy_mutex_lock(l);
	free(l);
}
