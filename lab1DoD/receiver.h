/*
*
*/

#include "shared.h"

#ifndef _RECEIVER_H
#define _RECEIVER_H

/*  Sets up a socket for receiving messages for the program
 *
 *  @returned    void pointer (needed to end the thread)
 */
void *receiver_init(void *arg);

/*  Contains the receiving loop:
 *   -listens to message in receiver socket and adds message to queue
 *    according to received message and current phase
 *
 *  @return:
 */
void receiver_listen(host_info *host, int sfd, list* messages);

/*  Accepts tcpConnection
 *
 *  @return: sfd to receiving socket
 */
int tcpAccept(int sfd);

/*  Reads n bytes from given socket
 *
 *  @return: message read
 */
char* readNBytes(int nrOfBytes, int protocolType, int sfd);

/*  Adds new message to queue according to given phase and recceived message
 *  - starts timer when election is over
 *
 *  @return:
 */
void electionProcedure(list* messages, char* msg, char* messageType
    , char* ownID, char* msgID, struct timeval* startTime);


/*  Forwards the message and stops records the laptime
 *
 *  @return:
 */
void messageRingProcedure(list* messages, char* msg, char* messageType
    , char* ownID, char* msgID, struct timeval* startTime, struct timeval* stopTime, int* laps);

#endif
