
#ifndef LIST_H
#define LIST_H
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <pthread.h>
#include <errno.h>
#include <string.h>


#ifndef _DATA
#define _DATA
typedef void *data;
#endif

#ifndef __MEMFREEFUNC
#define __MEMFREEFUNC
typedef void memFreeFunc(data);
#endif

typedef bool compareFunc(data, data);

typedef struct list_node{
	struct list_node *next;
	struct list_node *prev;
	data value;
} list_node;

typedef list_node* list_position;

typedef struct {
	list_node *top;
	list_node *bot;
	pthread_mutex_t lock;
	int size;
	memFreeFunc* freeFunc;
	compareFunc* compFunc;

} list;

#endif
