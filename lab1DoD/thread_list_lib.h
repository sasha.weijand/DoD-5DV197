#include "list_lib.h"

void init_mutex_lock(list* l);

void destroy_mutex_lock(list* l);

int list_size_t (list* l);

list_position list_first_t (list* l);

list_position list_last_t (list *l);

list_position list_next_t (list* l, list_position p);

list_position list_previous_t (list* l, list_position p);

list_position list_get_pos_at_t (list*l, int index);

list_position list_insert_t (list* l, data dp, list_position p);

list_position list_insert_at_t(list* l, data dp, int index);

bool list_isEmpty_t (list *l);

data list_inspect_t(list* l, list_position p);

data list_inspectat_t(list* l, int index);

list_position list_remove_t (list*l, list_position p);

list_position list_remove_at_t (list* l, int index);

void list_insertionSort_t(list *l);

void list_push_t (list* l, data dp);

list_position list_pop_t(list* l);

data list_peek_t (list* l);

void list_queue_t (list* l, data dp);

void list_dequeue_t (list* l);

void list_clear_t(list* l);
