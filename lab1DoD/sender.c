#include "sender.h"


void sendMessageFromQueue(list* messages, int sfd){
    char* msg = list_inspect_t(messages, list_first_t(messages));
    //fprintf(stderr, "SENDING MESSAGE:\n%s\n", msg);
    write(sfd, msg, BUFFERSIZE);
    list_dequeue_t(messages);
}

void startElection(list* messages, char* ownID){
    list_queue_t(messages, createMessage(ELECTION, ownID, NULL));
    fprintf(stderr, "STARTING ELECTION\n" );
}

void sendMessages (list* messages, char* ownID, int sfd) {
    while (!shouldExit) {
        if(phase == 0)
            phase = 1;

        if(phase == 1 && !is_nodeOnline){
            startElection(messages, ownID);
            sendMessageFromQueue(messages, sfd);
            sleep(1);
        }else{
            if(!list_isEmpty_t(messages))
                sendMessageFromQueue(messages, sfd);
        }
	}
}

void* sender_init(void* arg) {
    arguments* args = (arguments*) arg;
    host_info *serverHost = (host_info*) args->server;
    host_info *clientHost = (host_info*) args->client;
    list* messages = (list*) args->messages;

    struct addrinfo clientHints, *clientResult;
    setHints(&clientHints, clientHost);
    char* port = getHostPort(clientHost);
    clientResult = generateAddressInfo(clientHost->name, port, &clientHints, clientResult);
    int sfd = initSocket(clientResult, SENDER, clientHost);

    char* ownID = generateNodeID(*(serverHost->port));

    sendMessages(messages, ownID, sfd);

    free(ownID);
    free(port);
    free(clientResult);

    if(sfd != -1)
        close(sfd);
    exit(0);
}
