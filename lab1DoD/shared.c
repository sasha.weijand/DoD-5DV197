#include "shared.h"

void setHints(struct addrinfo* hints, host_info* host){

    memset(hints, 0, sizeof(struct addrinfo));
    hints->ai_socktype = (host->protocolType == UDP) ? SOCK_DGRAM : SOCK_STREAM;
    hints->ai_family = AF_INET;
    hints->ai_flags = AI_PASSIVE;
    hints->ai_protocol = 0;
    hints->ai_canonname = NULL;
    hints->ai_addr = NULL;
    hints->ai_next = NULL;
}

char* getHostPort(host_info* host){
    char *port = calloc(sizeof(char),16);
    sprintf(port, "%d", *host->port);
    return port;
}


struct addrinfo* generateAddressInfo(char* hostName, char* port, struct addrinfo* hints, struct addrinfo* result) {
    int retValue = getaddrinfo(hostName, port, hints, &result);
    if (retValue != 0) {
        fprintf(stderr, "Could not find host: %s\n", gai_strerror(retValue));
        exit(EXIT_FAILURE);
    }
    return result;
}

int getFQDN(char *fqdn, size_t n) {
    char hostname[n];
     int r = gethostname(hostname, n);
     if (r != 0) {
         return 1;
     }
     struct addrinfo h = {0};
     h.ai_family = AF_INET;
     h.ai_socktype = SOCK_STREAM;
     h.ai_flags = AI_CANONNAME;
     struct addrinfo *info;

     if (getaddrinfo(hostname, NULL, &h, &info) != 0) {
         return 2;
     }

     strncpy(fqdn, info->ai_canonname, n);
     freeaddrinfo(info);
     return 0;
}

char* generateHostname(){

  char tmpName[BUFFERSIZE];
  memset(tmpName, 0, BUFFERSIZE);
  int res = getFQDN(tmpName, BUFFERSIZE);

  int i = 0;
  while(tmpName[i] != 0){
      i++;
  }

  char* hostID = calloc(i, 1);
  strncpy(hostID, tmpName, i);

  return hostID;
}

char* generatePortString(int port){
    char* portString = calloc(1, BUFFERSIZE);
    sprintf(portString, "%d", port);

    return portString;
}

char* generateNodeID( int port){
    char* hostID = calloc(1, BUFFERSIZE);
    getFQDN(hostID, BUFFERSIZE);
    char* portString = generatePortString(port);
    strcat(hostID, ",");
    strcat(hostID, portString);
    free(portString);

    return hostID;
}

char* parseNodeIDfromMessage(char* message){
    char* msgCopy = strdup(message);

    char* delimiter = "\n";
    char* token;
    token = strtok(msgCopy, delimiter);
    token = strtok(NULL, delimiter);

    return token;
}

char* parseMessageTypeFromMessage(char* message){
    char* msgCopy = strdup(message);

    char* delimiter = "\n";
    char* token;

    return strtok(msgCopy, delimiter);
}

char* createMessage (char* messageType, char* nodeID, char* text) {
    char* msg = calloc (BUFFERSIZE, 1);

    strcat (msg, messageType);
    strcat (msg, "\n");

    strcat(msg, nodeID);
    strcat(msg, "\n");

    if (strcmp(messageType, MESSAGE) == 0) {
        strcat(msg, text);
        strcat(msg, "\n");
    }
    return msg;
}

void setSocketOptions (int sfd, int socketType) {
    int enableOption = 1;
    if (setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &enableOption, sizeof(enableOption))==-1) {
        perror("setsockopt");
        exit(1);
    }

    struct timeval tv;
    tv.tv_sec = 10;
    tv.tv_usec = 0;
    if (socketType == SENDER) {
        if (setsockopt(sfd, SOL_SOCKET, SO_SNDTIMEO, (char*)&tv, sizeof(struct timeval))) {
            perror("setsockopt");
            exit(1);
        }
    }
    else if (socketType == RECEIVER){
        if (setsockopt(sfd, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv, sizeof(struct timeval))) {
            perror("setsockopt");
            exit(1);
        }
    }

}

int initSocket (struct addrinfo* result, int socketType, host_info* host) {
    int sfd;

    struct addrinfo* rp;

    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (sfd == -1)
            continue;

        setSocketOptions (sfd, socketType);

        if(socketType == SENDER){
            while(1){
                if (connect(sfd, rp->ai_addr, rp->ai_addrlen) != -1){
                    return sfd;
                }else{
                    fprintf(stderr, "TRYING TO CONNECT TO: %s PORT: %d\n", host->name, *(host->port));
                    sleep(1);
                }
            }
        }

        if(socketType == RECEIVER){
            if (bind(sfd, rp->ai_addr, rp->ai_addrlen) == 0)
                return sfd;
        }
        fprintf(stderr, "%d\n", sfd );
        close(sfd);
    }

    if (rp == NULL) {
        perror("Could not bind socket");
        exit(EXIT_FAILURE);
    }
}

void freeHostInfo(host_info* host){
    free(host->name);
    free(host->port);
    free(host);
}
void freeArguments(arguments* args){
    list_free(args->messages);
    freeHostInfo(args->client);
    freeHostInfo(args->server);
    free(args);
}
