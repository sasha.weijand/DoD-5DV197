#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE 700
#endif

#include <signal.h>
#include <sys/types.h>
#include <stdlib.h>

#ifndef __SIGNAL_HANDLER_H
#define __SIGNAL_HANDLER_H


int shouldExit;

/*
 * This is the signal handling function,
 * which is called automatically when the signal SIGINT is given.
 */
void SIG_handler(int sig);

/*
 * Initiates the sigaction struct with a
 * given signal handler (see above), and some other options.
 */
struct sigaction* setSignalHandler(void(*handler)(int));
#endif
