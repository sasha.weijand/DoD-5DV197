/*
 *
 */

#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE
#endif


#include "receiver.h"
#include "sender.h"

#ifndef _NODE_H
#define _NODE_H

/*
 * Prints a message to stdout saying that
 * the parameters given to the program are invalid,
 * with usage instructions.
 */
void printInvalidParams(char *progName);


/*
 * Checks if the amount of arguments is correct,
 * and if the udp/tcp option is correctly given.
 */
void argumentCheck(int argc, char ** argv);

/* Takes the given string and returns an allocated integer of the
 * string representation.
 * @return A pointer to an integer respresantation of the given string
 */
int* getIntFromStr(char *givenStr);


/* Allocates a string and sets it to the current hostname.
 * @return The current hostname
 */
char* getCurrentHostName();

/*
 * Allocates a host_info struct and fills it with the needed information.
 * @return An initialized host_info struct
 */
host_info* generateHostInfo(char* nodeType, char* name, char* port);


/*
 * Allocates a createArgs struct and fills it with the command line arguments.
 * @return The initialized argument struct
 */
arguments* createArgs (char** argv);

#endif
