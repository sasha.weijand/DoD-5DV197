#include "signal_handler.h"
#include "stdio.h"


void SIG_handler(int sig){
	if (sig == SIGINT){
        shouldExit = 1;
    }

}


struct sigaction* setSignalHandler(void(*handler)(int)){
    struct sigaction* sigac = calloc(1, sizeof(struct sigaction));
    sigac->sa_handler = handler;
	sigac->sa_flags = SA_RESTART;
	sigaction(SIGINT, sigac, NULL);

    return sigac;
}
