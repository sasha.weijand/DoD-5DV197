#include "thread_list_lib.h"

void init_mutex_lock(list* l){
	if (pthread_mutex_init(&(l->lock), NULL) != 0){
		fprintf(stderr, "Error, initializing \n");
		exit(EXIT_FAILURE);
	}
}

void destroy_mutex_lock(list* l){
	if(pthread_mutex_destroy(&(l->lock)) != 0){
		fprintf(stderr, "%s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
}

int list_size_t (list* l){
	pthread_mutex_lock(&(l->lock));
	int size = l->size;
	pthread_mutex_unlock(&(l->lock));
	return 	size;
}

list_position list_first_t (list* l){
	pthread_mutex_lock(&(l->lock));
	list_position p = l->top->next;
	pthread_mutex_unlock(&(l->lock));
	return p;
}

list_position list_last_t (list *l){
	pthread_mutex_lock(&(l->lock));
	list_position p = l->bot->prev;
	pthread_mutex_unlock(&(l->lock));
	return p;
}

list_position list_next_t (list* l, list_position p){
	pthread_mutex_lock(&(l->lock));
	list_position next_position = p->next;
	pthread_mutex_unlock(&(l->lock));
	return next_position;
}

list_position list_previous_t (list* l, list_position p){
	pthread_mutex_lock(&(l->lock));
	list_position previous_position = p->prev;
	pthread_mutex_unlock(&(l->lock));
	return previous_position;
}


list_position list_get_pos_at_t (list*l, int index){
    pthread_mutex_lock(&(l->lock));
	list_position ret = list_get_pos_at(l, index);
    pthread_mutex_unlock(&(l->lock));
	return ret;
}

list_position list_insert_t (list* l, data dp, list_position p){
	pthread_mutex_lock(&(l->lock));
	list_position ret = list_insert(l, dp, p);
	pthread_mutex_unlock(&(l->lock));
	return ret;
}

list_position list_insert_at_t(list* l, data dp, int index){
    pthread_mutex_lock(&(l->lock));
	list_position ret = list_insert_at(l, dp, index);
    pthread_mutex_unlock(&(l->lock));
	return ret;
}

bool list_isEmpty_t (list *l){
	pthread_mutex_lock(&(l->lock));
	bool ret = list_isEmpty(l);
	pthread_mutex_unlock(&(l->lock));
	return ret;
}

data list_inspect_t(list* l, list_position p) {
	pthread_mutex_lock(&(l->lock));
	data value = p->value;
	pthread_mutex_unlock(&(l->lock));
	return value;
}

data list_inspect_at_t(list* l, int index) {
    pthread_mutex_lock(&(l->lock));
	data ret = list_inspect_at(l, index);
    pthread_mutex_unlock(&(l->lock));
	return ret;
}

list_position list_remove_t (list*l, list_position p){
	pthread_mutex_lock(&(l->lock));
	list_position ret = list_remove(l, p);
	pthread_mutex_unlock(&(l->lock));
	return ret;
}

list_position list_remove_at_t (list* l, int index){
    pthread_mutex_lock(&(l->lock));
	list_position ret = list_remove_at(l, index);
	pthread_mutex_unlock(&(l->lock));
	return ret;
}

void list_insertionSort_t(list *l){
    pthread_mutex_lock(&(l->lock));
	list_insertionSort(l);
    pthread_mutex_unlock(&(l->lock));
}


void list_push_t (list* l, data dp){
    pthread_mutex_lock(&(l->lock));
	list_push(l, dp);
    pthread_mutex_unlock(&(l->lock));
}

list_position list_pop_t(list* l){
    pthread_mutex_lock(&(l->lock));
	list_position ret = list_pop(l);
	pthread_mutex_unlock(&(l->lock));
	return ret;
}

data list_peek_t (list* l) {
    pthread_mutex_lock(&(l->lock));
    data d = list_inspect (l, list_first(l));
    pthread_mutex_unlock(&(l->lock));
    return d;
}

void list_queue_t (list* l, data dp){
	pthread_mutex_lock(&(l->lock));
    list_queue(l, dp);
    pthread_mutex_unlock(&(l->lock));
}

void list_dequeue_t (list* l){
	pthread_mutex_lock(&(l->lock));
    list_dequeue(l);
    pthread_mutex_unlock(&(l->lock));
}

void list_clear_t(list* l){
	pthread_mutex_lock(&(l->lock));
    list_clear(l);
    pthread_mutex_unlock(&(l->lock));
}
