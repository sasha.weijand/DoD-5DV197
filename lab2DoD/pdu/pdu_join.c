#include "pdu.h"


int pdu_join_calc_pduSize(int identityLength){
    int paddingLength = calc_padding_length (identityLength);

    return 4 + identityLength + paddingLength;
}

pdu_join* pdu_join_init(uint16_t identityLength){
    pdu_join* pdu = safe_calloc (sizeof (pdu_join));
    pdu -> def_pdu = pdu_create (OP_JOIN,
         &pdu_join_serialize, &pdu_join_deserialize,
         pdu_join_calc_pduSize(identityLength));

    pdu -> identityLength = safe_calloc (1);
    pdu -> padding_16b = safe_calloc (2);
    pdu -> identity = safe_calloc (identityLength + 1);

    return pdu;
}

pdu_join* pdu_join_create (uint8_t identityLength, uint8_t* identity) {
    pdu_join* pdu = pdu_join_init(identityLength);

    set_uint8(pdu -> identityLength, &identityLength);
    set_nBytes(pdu -> identity, identity, identityLength);
    return pdu;

}


void pdu_join_free (pdu_join* pdu) {
    pdu_free (pdu -> def_pdu);
    free (pdu -> identity);
    free (pdu -> identityLength);
    free (pdu -> padding_16b);
    free (pdu);
}



void* pdu_join_serialize (void* pdu_to_serialize) {
    pdu_join* pdu = (pdu_join*) pdu_to_serialize;

    uint8_t* buffer = safe_calloc (*(pdu->def_pdu->size));
    uint8_t identityLength = *(pdu -> identityLength);

    serialize_uint8(buffer, pdu -> def_pdu -> op);
    serialize_uint8(buffer+1, pdu -> identityLength);
    serialize_uint16(buffer+2, pdu -> padding_16b);
    serialize_nBytes(buffer+4, pdu -> identity, identityLength);

    return buffer;
}

bool validate_identityLength(uint8_t* identity, uint8_t length){
    for(int i = 0; i<length; i++){
        if(identity[i] == 0){
            fprintf(stderr, "identity length not equal to given length: found:%d expected:%d\n", i, length);
            return false;
        }
    }
    return true;
}

bool validate_padding(uint8_t* padding, int length){
    for(int i = 0; i<length; i++)
        if(padding[i] != 0){
            fprintf(stderr, "padding missing\n");
            return false;
        }
    return true;
}

bool validate_padding16b(uint16_t padding){
        if(padding != 0){
            fprintf(stderr, "padding 16b missing\n");
            return false;
        }
    return true;
}

void* pdu_join_deserialize (void* stream) {
    int FD = *(int*)stream;

    uint8_t identityLength = read_uint8(FD);
    uint16_t padding_16b = read_uint16(FD);
    uint8_t* identity = read_nBytes (FD, identityLength);
    uint8_t* padding = read_nBytes(FD, calc_padding_length(identityLength));

    if(!validate_identityLength(identity, identityLength)
        || !validate_padding(padding, calc_padding_length(identityLength))
        || !validate_padding16b(padding_16b)){
            validation_failed("PDU_JOIN");
            return NULL;
        }

    pdu_join* pdu = pdu_join_create (identityLength, identity);
    free(identity);
    free(padding);
    return pdu;
}

uint8_t* pdu_join_getIdentity(pdu_join* pdu){
    return (uint8_t*)(pdu->identity);
}
