//
// Created by et12swd on 2018-09-28.
//

#include "pdu.h"


pdu_slist* pdu_slist_init (uint16_t nrServers) {
    pdu_slist* pdu = safe_calloc(sizeof(pdu_slist));
    pdu->def_pdu = pdu_create(OP_SLIST, &pdu_slist_serialize, &pdu_slist_deserialize,0);
    pdu->padding = safe_calloc(1);
    pdu->nrOfServers = safe_calloc(2);
    pdu->servers = safe_calloc(nrServers * sizeof(server*));
    return pdu;
}

server* pdu_slist_init_server (uint16_t words) {
    server* s = safe_calloc(sizeof(server));
    s->address = safe_calloc(sizeof(uint32_t));
    s->port = safe_calloc(sizeof(uint16_t));
    s->nrOfClients = safe_calloc(sizeof(uint8_t));
    s->serverNameLength = safe_calloc(sizeof(uint8_t));
    s->serverName = safe_calloc(sizeof(uint32_t)*words);
    return s;
}


pdu_slist* pdu_slist_create (uint16_t nrServers) {
    pdu_slist* pdu = pdu_slist_init(nrServers);

    set_uint16(pdu->nrOfServers, &nrServers);
    return pdu;
}

void pdu_slist_free_server (server* s) {
    free(s->address);
    free(s->port);
    free(s->nrOfClients);
    free(s->serverNameLength);
    free(s->serverName);
    free(s);
}


void pdu_slist_free (pdu_slist* pdu) {

    printf("nr of servers %d\n", *pdu->nrOfServers);
    for (int i = 0; i <  *pdu->nrOfServers; i++) {
        pdu_slist_free_server (pdu->servers[i]);
    }

    free(pdu->servers);
    free(pdu->nrOfServers);
    free(pdu->padding);
    pdu_free(pdu->def_pdu);
    free(pdu);
}


void pdu_slist_print (pdu_slist* pdu) {
    // printf("nr of servers: %d\n", *pdu->nrOfServers);
    printf("List of servers:\n");
    printf("%-10s %-40s %s\n", "number", "server name", "port");
    for (int i = 0; i < *pdu->nrOfServers ; i++) {
        server* s = pdu->servers[i];
        printf ("%-10d ", i + 1);

        char sName[*s->serverNameLength + 1];
        memcpy (sName, s->serverName, *s->serverNameLength);
        memcpy (sName + *s->serverNameLength, "\0", 1);
        printf ("%-40s ", sName);
        printf ("%d \n", *s->port);
    }
}


char* pdu_slist_get_port (pdu_slist* slist, int num) {

    if (num > *slist->nrOfServers || num < 1) {
        fprintf(stderr, "No such server number!\n\n");
        return ""; //Error
    }
    server* s = slist->servers[num - 1];

    char* port = safe_calloc (6);
    sprintf(port, "%d", *s->port);

    return port;
}

char* pdu_slist_get_server_ip_address (pdu_slist* slist, int num) {

    if (num > *slist->nrOfServers || num < 1) {
        fprintf(stderr, "No such server number!\n\n");
        return ""; //Error
    }
    server* s = slist->servers[num - 1];
    uint8_t addr[5];
    serialize_uint32 (addr, s->address);
    addr[4] = 0;
    char* ipstr = safe_calloc (16);
    sprintf (ipstr, "%d.%d.%d.%d", addr[3], addr[2], addr[1], addr[0]);
    return ipstr;
}

void pdu_slist_add_server (pdu_slist* pdu, int index, uint8_t* address,
                           uint16_t port, uint8_t nrClients, uint8_t serverNameLength, char* serverName) {

    uint8_t add;
    serverNameLength % 4 == 0 ? (add = 0) : (add = 1);
    int amountOfWords = serverNameLength/4 + add;
    server* s = pdu_slist_init_server (amountOfWords);
    set_uint32 (s->address, (uint32_t*)address);
    set_uint16 (s->port, &port);
    set_uint8 (s->nrOfClients, &nrClients);
    set_uint8 (s->serverNameLength, &serverNameLength);
    set_nBytes (s->serverName, serverName, amountOfWords * 4);

    pdu->servers[index] = s;
}



void pdu_slist_add_servers (pdu_slist* pdu, uint8_t** addresses, uint16_t* ports,
        uint8_t* arrayOfNrOfClients, uint8_t* serverNameLengths, char** serverNames) {

    for (int i = 0; i < *(pdu->nrOfServers); i++) {
        pdu_slist_add_server(pdu, i, addresses[i], ports[i], arrayOfNrOfClients[i], serverNameLengths[i], serverNames[i]);
    }
}


size_t getBufSize (pdu_slist* pdu) {
    size_t bufsize = (size_t)(4 + (8 * *pdu->nrOfServers));

    for (int i = 0; i < *pdu->nrOfServers ; i++) {
        int sLength = *pdu->servers[i]->serverNameLength;
        bufsize += sLength + calc_padding_length (sLength);
    }
    printf ("returning bufsize %d\n", (int) bufsize);
    return bufsize;
}


void* pdu_slist_serialize (void* pduToSerialize) {
    pdu_slist* pdu = (pdu_slist*) pduToSerialize;
    size_t bufsize = getBufSize (pdu);
    *(pdu->def_pdu->size) = (int)bufsize;
    uint8_t* buffer = safe_calloc(bufsize);
    if (buffer == NULL) {
        fprintf (stderr, "serialize: error allocating buffer\n");
        exit (EXIT_FAILURE);
    }

    serialize_uint8 (buffer, pdu->def_pdu->op);
    serialize_uint8 (buffer + 1, pdu->padding);
    serialize_uint16 (buffer + 2, pdu->nrOfServers);
    for (int i = 0, bufIndex = 4; i < *pdu->nrOfServers, bufIndex < bufsize ; i++) {
        server* s = pdu->servers[i];
        serialize_nBytes (buffer + bufIndex, s->address, 4);
        bufIndex+= 4;
        serialize_uint16 (buffer + bufIndex, s->port);
        bufIndex+=2;
        serialize_uint8 (buffer + bufIndex, s->nrOfClients);
        bufIndex++;
        serialize_uint8 (buffer + bufIndex, s->serverNameLength);
        bufIndex++;
        serialize_nBytes(buffer + bufIndex, s->serverName, *(s->serverNameLength));
        bufIndex += *(s->serverNameLength) + calc_padding_length (*(s->serverNameLength));
    }
    return buffer;
}

bool pdu_slist_validate (void* stream) {
    uint8_t* buf = (uint8_t*) stream;
    if (buf[0] != OP_SLIST) {
        return false;
    }
    return true;
}


void* pdu_slist_deserialize (void* byte_stream) {

    /*if (!pdu_slist_validate (byte_stream)) {
        validation_failed ("pdu_slist");
    }*/

    int FD = *(int*)byte_stream;
    uint8_t pad = read_uint8 (FD);
    uint16_t nrServers = read_uint16 (FD);
    //printf("nr servers %d\n", nrServers);
    uint8_t* addresses[nrServers];
    uint16_t ports[nrServers];
    uint8_t nrClients[nrServers];
    uint8_t serverNameLengths[nrServers];
    uint8_t* serverNames[nrServers];
    for (int i = 0; i < nrServers; i++) {
        addresses[i] = read_nBytes (FD, 4);
        //printf("address %d: %u\n", i, addresses[i]);
        ports[i] = read_uint16 (FD);
        //printf("port %d: %u\n", i, ports[i]);
        nrClients[i] = read_uint8 (FD);
        //printf("nrClients %d: %u\n", i, nrClients[i]);
        serverNameLengths[i] = read_uint8 (FD);
        //printf("server name length %d: %u\n", i, serverNameLengths[i]);
        int padLength = calc_padding_length(serverNameLengths[i]);
        //printf("padding length: %d\n", padLength);
        serverNames[i] = read_nBytes (FD, serverNameLengths[i] + padLength);
        //printf("server name %d: %u\n", i, serverNames[i]);
    }

    pdu_slist* pdu = pdu_slist_create (nrServers);
    pdu_slist_add_servers (pdu, addresses, ports, nrClients,
                           serverNameLengths, (char**) serverNames);

    for (int j = 0; j < nrServers; j++) {
        free(serverNames[j]);
        free(addresses[j]);
    }

    return pdu;
}
