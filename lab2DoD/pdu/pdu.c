#include "pdu.h"

pdu* pdu_create(uint8_t op, serialize* ser, deserialize* deSer, int size){
    pdu* pdu = safe_calloc(sizeof(struct pdu));
    pdu->ser = ser;
    pdu->deSer = deSer;
    pdu->op = safe_calloc(1);
    pdu->size = safe_calloc(sizeof(int));
    set_uint8(pdu->op, &op);
    memcpy(pdu->size, &size, sizeof(int));
    return pdu;
}

void pdu_any_free (pdu* pduToFree) {
    int op = pdu_getOP(pduToFree);
    switch (op) {
        case OP_REG:
            pdu_reg_free ((pdu_reg*) pduToFree);
            break;
        case OP_ACK:
            pdu_ack_free ((pdu_ack*) pduToFree);
            break;
        case OP_ALIVE:
            pdu_alive_free ((pdu_alive*) pduToFree);
            break;
        case OP_GETLIST:
            pdu_getlist_free ((pdu_getlist*) pduToFree);
            break;
        case OP_SLIST:
            pdu_slist_free ((pdu_slist*) pduToFree);
            break;
        case OP_MESS:
            pdu_mess_free ((pdu_mess*) pduToFree);
            break;
        case OP_QUIT:
            pdu_quit_free ((pdu_quit*) pduToFree);
            break;
        case OP_JOIN:
            pdu_join_free ((pdu_join*) pduToFree);
            break;
        case OP_PJOIN:
            pdu_pjoin_free ((pdu_pjoin*) pduToFree);
            break;
        case OP_PLEAVE:
            pdu_pleave_free ((pdu_pleave*) pduToFree);
            break;
        case OP_PARTICIPANTS:
            pdu_participants_free ((pdu_participants*) pduToFree);
            break;
        case OP_NOTREG:
            pdu_notreg_free ((pdu_notreg*) pduToFree);
            break;
        default:
            break;
    }
}

void* pdu_deserialize(int FD){
    uint8_t op = read_uint8(FD);


    switch (op) {
        case OP_REG:
            return pdu_reg_deserialize(&FD);
            break;

        case OP_ACK:
            return pdu_ack_deserialize(&FD);
            break;

        case OP_ALIVE:
            return pdu_alive_deserialize(&FD);
            break;

        case OP_NOTREG:
            return pdu_notreg_deserialize(&FD);
            break;

        case OP_GETLIST:
            return pdu_getlist_deserialize(&FD);
            break;

        case OP_SLIST:
            return pdu_slist_deserialize(&FD);
            break;

        case OP_MESS:
            return pdu_mess_deserialize(&FD);
            break;

        case OP_QUIT:
            return pdu_quit_deserialize(&FD);
            break;

        case OP_JOIN:
            return pdu_join_deserialize(&FD);
            break;

        case OP_PJOIN:
            return pdu_pjoin_deserialize(&FD);
            break;

        case OP_PLEAVE:
            return pdu_pleave_deserialize(&FD);
            break;

        case OP_PARTICIPANTS:
            return pdu_participants_deserialize(&FD);
            break;

        default:
            fprintf(stderr, "invalid OP-code read: %d\n", op);
            return NULL;
        }

}

void pdu_setSize(void* tmpPdu, int size){
    struct pdu* pdu = ((pdu_reg*)tmpPdu)->def_pdu;
    memcpy(pdu->size, &size, sizeof(int));
}

int pdu_getSize(void* tmpPdu){
    struct pdu* pdu = ((pdu_reg*)tmpPdu)->def_pdu;
    return *(pdu->size);
}

int pdu_getOP(void* tmpPdu){
    struct pdu* pdu = ((pdu_pjoin*)tmpPdu)->def_pdu;
    return *(pdu->op);
}

uint8_t* pdu_serialize(void* pduToWrite){
    struct pdu* pdu = ((pdu_reg*)pduToWrite)->def_pdu;
    return (uint8_t*)pdu->ser(pduToWrite);
}

void pdu_free(pdu* pdu){
    free(pdu->op);
    free(pdu->size);
    free(pdu);
}
