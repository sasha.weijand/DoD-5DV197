
#include <sys/poll.h>
#include "pdu_util.h"

void* safe_calloc (int size) {
    void* ptr = calloc (size, 1);
    if (ptr == NULL) {
        exit(EXIT_FAILURE);
    }
    return ptr;
}


int calc_padding_length(int length){
    if (length%4 == 0) {
        return 0;
    }
    return (4 - length%4);
}

uint16_t parse_uint16(uint8_t* buffer){
    uint16_t* value_2bytes = (uint16_t*)buffer;
    return ntohs(*value_2bytes);
}

uint32_t parse_uint32 (uint8_t* buffer) {
    uint32_t* value_4bytes = (uint32_t*)buffer;
    return ntohl(*value_4bytes);
}

uint8_t* parse_nBytes (uint8_t* byte_stream, int bytes) {
    uint8_t* buffer = safe_calloc (bytes);

    memcpy(buffer, byte_stream, bytes);
    return buffer;
}

void set_uint8(uint8_t* destination, uint8_t* source){
    memcpy(destination, source,1);

}

void set_uint16(uint16_t* destination, uint16_t* source){
    memcpy(destination, source,2);

}

void set_uint32(uint32_t* destination, uint32_t* source) {
    memcpy(destination, source, 4);
}

void set_nBytes(void* tmpDestination, void* tmpSource, uint16_t bytes){
    uint8_t* destination = tmpDestination;
    uint8_t* source = tmpSource;
    memcpy(destination, source, bytes);
}


uint8_t* read_nBytes(int FD, int bytes){
    uint8_t* buffer = safe_calloc(bytes + 1);
    int offset = 0;
    int nread = 0;
    while (offset < bytes){
        //fprintf(stderr, "OFFSET: %d\n", offset);
        nread = read(FD, buffer + offset, bytes-offset);
        if( nread  == -1 || nread == 0){
            if(nread == -1)
                perror("read_nBytes");
            else
                fprintf(stderr, "Timeout received, all bytes not available\n");

            free(buffer);
            return NULL;
        }else{
            offset += nread;
        }
    }

    return buffer;
}

uint8_t read_uint8(int FD){
    uint8_t* buffer = (read_nBytes(FD, 1));
    uint8_t value = *buffer;
    free(buffer);
    return value;
}

uint16_t read_uint16(int FD){
    uint8_t* buffer = read_nBytes(FD, 2);
    uint16_t value = ntohs(*(uint16_t*)buffer);
    free(buffer);
    return value;
}

uint32_t read_uint32(int FD){
    uint8_t* buffer = read_nBytes(FD, 4);
    uint32_t  value = ntohl(*(uint32_t*)buffer);
    free(buffer);
    return value;
}

void serialize_uint8(uint8_t* destination, uint8_t* toSerialize){
    memcpy (destination, toSerialize, 1);
}

void serialize_uint16(uint8_t* destination, uint16_t* toSerialize){
    *(toSerialize) = htons(*(toSerialize));
    memcpy(destination, toSerialize, 2);
    *(toSerialize) = ntohs(*(toSerialize));
}

void serialize_uint32(uint8_t* destination, uint32_t* toSerialize){
    *(toSerialize) = htonl(*(toSerialize));
    memcpy(destination, toSerialize, 4);
    *(toSerialize) = ntohl(*(toSerialize));
}

void serialize_nBytes(void* tmpDestination, void* tmpToSerialize, uint16_t bytes){
    uint8_t* destination = tmpDestination;
    uint8_t* toSerialize = tmpToSerialize;
    memcpy(destination, toSerialize, bytes);
}

void validation_failed(char* pduType){
    fprintf (stderr, "Validation failed: %s\n", pduType);
}

int open_stream(char* filepath){
    int FD = open(filepath, O_RDONLY);
    if( FD == -1){
        fprintf(stderr, "Could not open file: %s", filepath);
        exit(EXIT_FAILURE);
    }
    return FD;
}



bool assert_uint8 (uint8_t found, uint8_t expected, char* testname) {
    if(found != expected){
        fprintf(stderr, "%s failed: expected:%d found:%d\n",testname, expected, found);
        return false;
    }
    return true;
}

bool assert_uint16 (uint16_t found, uint16_t expected, char* testname) {
    if(found != expected){
        fprintf(stderr, "%s failed: expected:%d found:%d\n",testname, expected, found);
        return false;
    }
    return true;
}

bool assert_uint32 (uint32_t found, uint32_t expected, char* testname) {
    if(found != expected){
        fprintf(stderr, "%s failed: expected:%d found:%d\n",testname, expected, found);
        return false;
    }
    return true;
}

bool assert_nBytes(void* tmpFound, void* tmpExpected, uint16_t length, char* testname){
    uint8_t* found = tmpFound;
    uint8_t* expected = tmpExpected;
    for(int i = 0; i<length; i++){
        if(found[i] != expected[i]){
            fprintf(stderr, "%s failed: expected:%s found:%s\n",testname, (char*)expected, (char*)found);
            return false;
        }
    }
    return true;
}
