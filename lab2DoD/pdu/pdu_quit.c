#include "pdu.h"

pdu_quit* pdu_quit_create () {
    pdu_quit* pdu = safe_calloc (sizeof (pdu_quit));
    pdu -> def_pdu = pdu_create (OP_QUIT, &pdu_quit_serialize, &pdu_quit_deserialize, 4);

    pdu -> padding = safe_calloc(3);
}


void pdu_quit_free (pdu_quit* pdu) {
    pdu_free (pdu -> def_pdu);
    free (pdu -> padding);
    free (pdu);
}


void* pdu_quit_serialize (void* pdu_to_serialize) {
    pdu_quit* pdu = (pdu_quit*) pdu_to_serialize;
    uint8_t* buffer = safe_calloc (4);

    serialize_uint8(buffer, pdu -> def_pdu -> op);
    serialize_nBytes(buffer+1, pdu -> padding, 3);

    return buffer;
}

bool pdu_quit_validate_padding (uint8_t* padding) {
    for(int i = 0; i<3; i++ )
        if(padding[i] != 0){
            fprintf (stderr, "Padding missing\n");
            return false;
        }
    return true;
}

void* pdu_quit_deserialize (void* byte_stream) {
    int FD = *(int*)byte_stream;

    uint8_t* padding = read_nBytes(FD, 3);

    if (!pdu_quit_validate_padding (padding)){
        validation_failed("PDU_QUIT");
        return NULL;
    }

    pdu_quit* pdu = pdu_quit_create ();
    free(padding);
    return pdu;
}
