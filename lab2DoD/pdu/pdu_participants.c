#include "pdu.h"

void cpy_identites(void* destination, void* identities, uint16_t length){
    uint8_t* dest = destination;
    uint8_t* ids = identities;

    for(int i = 0; i < length; i++)
        dest[i] = ids[i];
}

void set_identities(uint32_t* destination, uint8_t* identities, uint16_t length){
    cpy_identites(destination, identities, length);
}

int pdu_participants_calc_pduSize(uint16_t length){
    uint16_t paddingLength = calc_padding_length (length);

    return 4 + length + paddingLength;
}

pdu_participants* pdu_participants_init(uint16_t length){
    pdu_participants* pdu = safe_calloc (sizeof (pdu_participants));
    pdu -> def_pdu = pdu_create (OP_PARTICIPANTS,
        &pdu_participants_serialize, &pdu_participants_deserialize,
        pdu_participants_calc_pduSize(length));

    pdu -> numberOfIdentities = safe_calloc (1);
    pdu -> length = safe_calloc (2);
    pdu -> identities = safe_calloc(length);
}


pdu_participants* pdu_participants_create (uint8_t numberOfIdentities, uint16_t length, uint8_t* identities) {
    pdu_participants* pdu = pdu_participants_init(length);

    set_uint8(pdu -> numberOfIdentities, &numberOfIdentities);
    set_uint16(pdu -> length, &length);
    set_identities(pdu -> identities, identities, length);

    return pdu;
}

void pdu_participants_free (pdu_participants* pdu) {
    pdu_free (pdu -> def_pdu);
    free (pdu -> numberOfIdentities);
    free (pdu -> length);
    free (pdu -> identities);
    free (pdu);
}

void serialize_identities(uint8_t* destination, uint32_t* identities, uint16_t length){
    cpy_identites(destination, identities, length);
}

void* pdu_participants_serialize (void* pdu_to_serialize) {
    pdu_participants* pdu = (pdu_participants*) pdu_to_serialize;
    uint8_t* buffer = safe_calloc(*(pdu->def_pdu->size));
    uint16_t length = *(pdu -> length);

    serialize_uint8(buffer, pdu -> def_pdu -> op);
    serialize_uint8(buffer+1, pdu -> numberOfIdentities);
    serialize_uint16(buffer+2, pdu -> length);
    serialize_identities(buffer+4, pdu -> identities, length);

    return buffer;
}

void* pdu_participants_deserialize (void* stream) {
    int FD = *(int*)stream;

    uint8_t numberOfIdentities = read_uint8(FD);
    uint16_t length = read_uint16(FD);
    uint8_t* identities = read_nBytes(FD, length + calc_padding_length(length));


    pdu_participants* pdu = pdu_participants_create (numberOfIdentities, length, identities);
    free(identities);
    return pdu;
}

uint8_t pdu_participants_getNumberOfParticipants(pdu_participants* pdu){
    return *pdu->numberOfIdentities;
}

uint8_t* pdu_participants_getParticipants(pdu_participants* pdu){
    return (uint8_t*)pdu->identities;
}
