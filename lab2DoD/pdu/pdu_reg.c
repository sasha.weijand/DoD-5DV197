#include "pdu.h"

int pdu_reg_calc_pduSize(uint8_t serverNameLength){
    int paddingLength = calc_padding_length (serverNameLength);

    return 4 + serverNameLength + paddingLength;
}

pdu_reg* pdu_reg_init(uint8_t serverNameLength){
    pdu_reg* pdu = safe_calloc (sizeof (pdu_reg));
    pdu -> def_pdu = pdu_create (OP_REG,
         &pdu_reg_serialize, &pdu_reg_deserialize,
         pdu_reg_calc_pduSize(serverNameLength));

    pdu -> serverNameLength = safe_calloc(1);
    pdu -> serverName = safe_calloc(serverNameLength +1);
    pdu -> port = safe_calloc(2);

    return pdu;
}

pdu_reg* pdu_reg_create (uint8_t serverNameLength, uint8_t* serverName, uint16_t port) {
    pdu_reg* pdu = pdu_reg_init(serverNameLength);

    set_uint8(pdu -> serverNameLength, &serverNameLength);
    set_nBytes(pdu -> serverName, serverName, serverNameLength);
    set_uint16(pdu -> port, &port);

    return pdu;
}


void pdu_reg_free (pdu_reg* pdu) {
    pdu_free (pdu -> def_pdu);
    free (pdu -> serverNameLength);
    free (pdu -> port);
    free (pdu -> serverName);
    free (pdu);
}



void* pdu_reg_serialize (void* pdu_to_serialize) {
    pdu_reg* pdu = (pdu_reg*) pdu_to_serialize;
    uint8_t* buffer = safe_calloc(*(pdu->def_pdu->size));

    serialize_uint8(buffer, pdu->def_pdu->op);
    serialize_uint8(buffer+1, pdu->serverNameLength);
    serialize_uint16(buffer+2, pdu->port);
    serialize_nBytes(buffer+4, pdu->serverName, *(pdu->serverNameLength));

    return buffer;
}

bool pdu_reg_validate_servernameLength(uint8_t* servername, uint8_t length){
    for(int i = 0; i<length; i++)
        if(servername[i] == 0){
            fprintf(stderr, "Server name length not equal to given length: found:%d expected:%d\n", i, length);
            return false;
        }
    return true;
}

bool pdu_reg_validate_padding(uint8_t* padding, int length){

    for(int i = 0; i<length; i++)
        if(padding[i] != 0){
            fprintf(stderr, "%d\n", padding[i]);
            fprintf(stderr, "missing padding\n");
            return false;
        }
    return true;
}


void* pdu_reg_deserialize (void* stream) {
    int FD = *(int*)stream;

    uint8_t serverLength = read_uint8(FD);
    uint16_t port = read_uint16(FD);
    uint8_t* serverName = read_nBytes(FD, serverLength);
    uint8_t* padding = read_nBytes(FD, calc_padding_length(serverLength));

    if(!pdu_reg_validate_servernameLength(serverName, serverLength)
        || !pdu_reg_validate_padding(padding, calc_padding_length(serverLength))){
            validation_failed("PDU_REG");
            return NULL;
        }

    pdu_reg* pdu =  pdu_reg_create (serverLength, serverName, port);
    free(serverName);
    free(padding);
    return pdu;
}
