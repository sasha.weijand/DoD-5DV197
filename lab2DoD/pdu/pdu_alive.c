#include "pdu.h"


pdu_alive* pdu_alive_init(){
    pdu_alive* pdu = safe_calloc (sizeof (pdu_alive));
    pdu -> def_pdu = pdu_create (OP_ALIVE, &pdu_alive_serialize, &pdu_alive_deserialize, 4);

    pdu -> nrOfClients = safe_calloc (1);
    pdu -> ID = safe_calloc (2);

    return pdu;
}


pdu_alive* pdu_alive_create (uint8_t nrOfClients, uint16_t ID) {
    pdu_alive* pdu = pdu_alive_init();

    set_uint8(pdu -> nrOfClients, &nrOfClients);
    set_uint16(pdu -> ID, &ID);

    return pdu;
}


void pdu_alive_free (pdu_alive* pdu) {
    pdu_free (pdu -> def_pdu);
    free (pdu -> nrOfClients);
    free (pdu -> ID);
    free (pdu);
}

void* pdu_alive_serialize (void* pduToSerialize) {
    pdu_alive* pdu = (pdu_alive*) pduToSerialize;
    uint8_t* buffer = safe_calloc (4);

    serialize_uint8(buffer, pdu -> def_pdu -> op);
    serialize_uint8(buffer+1, pdu -> nrOfClients);
    serialize_uint16(buffer+2, pdu -> ID);


    return buffer;
}

void* pdu_alive_deserialize (void* stream) {
    int FD = *(int*) stream;

    uint8_t nrOfClients = read_uint8(FD);
    uint16_t ID = read_uint16(FD);

    if (ID == 0){
        validation_failed("PDU_ALIVE");
        return NULL;
    }


    return pdu_alive_create (nrOfClients, ID);
}

uint16_t pdu_alive_getID(pdu_alive* pdu){
    return *(pdu->ID);
}
