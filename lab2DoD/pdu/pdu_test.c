#include"pdu_test.h"

bool pdu_reg_test(){
    return pdu_reg_serialize_test() && pdu_reg_deserialize_test();
}

bool pdu_join_test(){
    return pdu_join_serialize_test() && pdu_join_deserialize_test();
}

bool pdu_participants_test(){
    return pdu_participants_serialize_test() && pdu_participants_deserialize_test();

}

bool pdu_alive_test(){
    return pdu_alive_serialize_test() && pdu_alive_deserialize_test();
}

bool pdu_ack_test(){
    return pdu_ack_serialize_test() && pdu_ack_deserialize_test();
}


bool pdu_getlist_test(){
    return pdu_getlist_serialize_test() && pdu_getlist_deserialize_test();
}


bool pdu_quit_test(){
    return pdu_quit_serialize_test() && pdu_quit_deserialize_test();
}


bool pdu_pleave_test(){
    return pdu_pleave_serialize_test() && pdu_pleave_deserialize_test();
}

bool pdu_pjoin_test(){
    return pdu_pjoin_serialize_test() && pdu_pjoin_deserialize_test();
}

bool pdu_notreg_test(){
    return pdu_notreg_serialize_test() && pdu_notreg_deserialize_test();
}

bool pdu_mess_test(){
    return pdu_mess_serialize_test() && pdu_mess_deserialize_test();

}

bool pdu_slist_test () {

    return pdu_slist_serialize_test () && pdu_slist_deserialize_test ();
}

bool pdu_reg_serialize_test () {
    uint8_t serverNameLength = 5;
    uint8_t* serverName = "testa";
    uint16_t port = 1337;
    uint8_t* serverNamePadding = "\0\0\0";

    pdu_reg* pdu = pdu_reg_create (serverNameLength, serverName, port);
    uint8_t* buffer = pdu_reg_serialize (pdu);

    assert_uint8 (buffer[0], OP_REG
        , "PDU_REG: SERIALIZE: OP-code test");

    assert_uint8 (buffer[1], serverNameLength
        , "PDU_REG: SERIALIZE: server name length test");

    assert_uint16 (parse_uint16(buffer+2), port
        , "PDU_REG: SERIALIZE: port test");

    uint8_t* serverNameBuffer = parse_nBytes(buffer+4, buffer[1]);
    assert_nBytes (serverNameBuffer, serverName, buffer[1]
    , "PDU_REG: SERIALIZE: server name test");
    free(serverNameBuffer);

    uint8_t* padBuffer = parse_nBytes(buffer+4+5, 3);
    assert_nBytes(padBuffer, serverNamePadding, 3
    , "PDU_REG: SERIALIZE: server name padding test");
    free(padBuffer);

    free (buffer);
    pdu_reg_free(pdu);

    return true;
}

bool pdu_reg_deserialize_test() {
    uint8_t serverLength = 5;
    uint8_t* serverName = "testa";
    uint16_t port = 1337;

    int FD = open_stream("PduTests/pdu_reg_valid.pdu");
    int OP = read_uint8(FD);
    pdu_reg* pdu_mock = pdu_reg_deserialize(&FD);

    assert_uint8(*(pdu_mock->serverNameLength), serverLength
    , "PDU_REG: DESERIALIZE: server name length test");

    assert_uint16(*(pdu_mock->port), port
    , "PDU_REG: DESERIALIZE: port test");

    assert_nBytes(pdu_mock->serverName, serverName, serverLength
    , "PDU_REG: DESERIALIZE: server name test");

    pdu_reg_free(pdu_mock);
    close(FD);
    return true;
}


bool pdu_participants_serialize_test () {
    int numberOfIdentities = 3;
    uint16_t length = 15;
    uint8_t* identities = "tes\0testa\0test\0";
    uint8_t* padding = "\0";

    pdu_participants* pdu = pdu_participants_create (numberOfIdentities, length, identities);

    uint8_t* buffer = pdu_participants_serialize (pdu);

    assert_uint8 (buffer[0], OP_PARTICIPANTS
        , "PDU_PARTICIPANTS: SERIALIZE: OP-code test");

    assert_uint8 (buffer[1], numberOfIdentities
        , "PDU_PARTICIPANTS: SERIALIZE: number of identities test");

    assert_uint16 (parse_uint16(buffer+2), length
        , "PDU_PARTICIPANTS: SERIALIZE: identies length test");

    assert_nBytes (buffer+4, identities, length
        , "PDU_PARTICIPANTS: SERIALIZE: identities test");

    assert_nBytes(buffer+4+length, padding, 1
        , "PDU_PARTICIPANTS: SERIALIZE: identites padding test");

    free (buffer);
    pdu_participants_free (pdu);
    return true;
}


bool pdu_participants_deserialize_test () {
    uint8_t numberOfIdentities = 3;
    uint16_t length = 9;
    uint8_t* identities = "a\0ab\0abc\0";

    int FD = open_stream("PduTests/pdu_participants_valid.pdu");
    uint8_t OP = read_uint8(FD);
    pdu_participants* pdu_mock = pdu_participants_deserialize(&FD);

    assert_uint8(*(pdu_mock->numberOfIdentities), numberOfIdentities
    , "PDU_PARTICIPANTS: DESERIALIZE: number of identities test");

    assert_uint16(*(pdu_mock->length), length
    , "PDU_PARTICIPANTS: DESERIALIZE: identities length test");

    assert_nBytes(pdu_mock->identities, identities, length
    , "PDU_PARTICIPANTS: DESERIALIZE: identities test");

    pdu_participants_free(pdu_mock);
    close(FD);
    return true;
}


bool pdu_join_serialize_test () {
    uint8_t identityLength = 5;
    char* identity = "testa";
    uint8_t* identityPadding = "\0\0\0";
    pdu_join* pdu = pdu_join_create (identityLength, identity);
    uint8_t* buffer = pdu_join_serialize (pdu);

    assert_uint8 (buffer[0], OP_JOIN
        , "PDU_JOIN: SERIALIZE: OP-code test");

    assert_uint8 (buffer[1], identityLength
        , "PDU_JOIN: SERIALIZE: identity length test");

    assert_uint16 (parse_uint16(buffer+2), 0
        , "PDU_JOIN: SERIALIZE: padding test");

    assert_nBytes (buffer+4, identity, identityLength
        , "PDU_JOIN: SERIALIZE: identity test");

    assert_nBytes (buffer+4+identityLength, identityPadding, 3
        , "PDU_JOIN: SERIALIZE: identity padding");

    free (buffer);
    pdu_join_free (pdu);
    return true;
}

bool pdu_join_deserialize_test () {
    uint8_t identityLength = 5;
    uint8_t* identity = "testa";
    int FD = open_stream("PduTests/pdu_join_valid.pdu");
    uint8_t OP =read_uint8(FD);
    pdu_join* pdu_mock = pdu_join_deserialize(&FD);

    assert_uint8(*(pdu_mock->identityLength), identityLength
    , "PDU_JOIN: DESERIALIZE: identity length test");

    assert_uint16(*(pdu_mock->padding_16b), 0
    , "PDU_JOIN: DESERIALIZE: padding 16b test");

    assert_nBytes(pdu_mock->identity, identity, identityLength
    , "PDU_JOIN: DESERIALIZE: identity test");

    pdu_join_free(pdu_mock);
    close(FD);
    return true;
}


bool pdu_alive_serialize_test () {
    uint8_t nrOfClients = 3;
    uint16_t ID = 55;
    pdu_alive* pdu = pdu_alive_create (nrOfClients, ID);
    uint8_t* buffer = pdu_alive_serialize (pdu);

    assert_uint8 (buffer[0], OP_ALIVE
            , "PDU_REG: SERIALIZE: OP-code test");

    assert_uint8 (buffer[1], nrOfClients
            , "PDU_REG: SERIALIZE: nr of clients test");

    assert_uint16 (parse_uint16(buffer + 2), ID
            , "PDU_REG: SERIALIZE: ID test");
    free (buffer);

    pdu_alive_free (pdu);
    return true;
}


bool pdu_alive_deserialize_test () {
    uint8_t nrOfClients = 3;
    uint16_t ID = 55;
    int FD = open_stream("PduTests/pdu_alive_valid.pdu");
    int OP = read_uint8(FD);

    pdu_alive* pdu_mock = pdu_alive_deserialize(&FD);
    assert_uint8(*(pdu_mock -> nrOfClients), nrOfClients, "PDU_ALIVE: DESERIALIZE: nr of clients test");
    assert_uint16(*(pdu_mock -> ID), ID, "PDU_ALIVE: DESERIALIZE: ID test");

    pdu_alive_free(pdu_mock);
    close(FD);
    return true;
}


bool pdu_ack_serialize_test () {
    uint16_t ID = 1337;
    pdu_ack* pdu = pdu_ack_create (ID);
    uint8_t* buffer = pdu_ack_serialize (pdu);

    assert_uint8 (buffer[0], OP_ACK
        , "PDU_ACK: SERIALIZE: OP-code test");

    assert_uint8 (buffer[1], 0
        , "PDU_ACK: SERIALIZE: padding test");

    assert_uint16 (parse_uint16(buffer +2), ID
        , "PDU_ACK: SERIALIZE: ID test");

    free (buffer);
    pdu_ack_free (pdu);
    return true;
}

bool pdu_ack_deserialize_test () {
    uint16_t ID = 1337;

    int FD = open_stream("PduTests/pdu_ack_valid.pdu");
    uint8_t OP = read_uint8(FD);
    pdu_ack* pdu_mock = pdu_ack_deserialize(&FD);

    assert_uint8(*(pdu_mock->padding), 0
    , "PDU_ACK: DESERIALIZE: pading test");

    assert_uint16(*(pdu_mock->ID), ID
    , "PDU_ACK: DESERIALIZE: ID test");

    pdu_ack_free(pdu_mock);
    close(FD);
    return true;
}


bool pdu_getlist_serialize_test () {
    pdu_getlist* pdu = pdu_getlist_create ();
    uint8_t* buffer = pdu_getlist_serialize (pdu);

    assert_uint8 (buffer[0], OP_GETLIST
        , "PDU_GETLIST: SERIALIZE: OP-code test");

    for(int i = 1; i<4; i++)
        assert_uint8 (buffer[i], 0
            , "PDU_GETLIST: SERIALIZE: padding test");

    free (buffer);
    pdu_getlist_free (pdu);
    return true;
}

bool pdu_getlist_deserialize_test(){

    uint8_t padding[3] = {0,0,0};
    int FD = open_stream("PduTests/pdu_getlist_valid.pdu");
    uint8_t OP = read_uint8(FD);
    pdu_getlist* pdu_mock = pdu_getlist_deserialize(&FD);
    assert_nBytes(pdu_mock->padding, padding, 3
    , "PDU_GETLIST: DESERIALIZE: padding test");

    pdu_getlist_free(pdu_mock);
    close(FD);
    return true;
}



bool pdu_quit_serialize_test () {
    pdu_quit* pdu = pdu_quit_create ();
    uint8_t* buffer = pdu_quit_serialize (pdu);

    assert_uint8 (buffer[0], OP_QUIT
        , "PDU_QUIT: SERIALIZE: OP-code test");

    for(int i = 1; i<4; i++)
        assert_uint8 (buffer[i], 0
            , "PDU_QUIT: SERIALIZE: padding test");

    free (buffer);
    pdu_quit_free (pdu);
    return true;
}

bool pdu_quit_deserialize_test(){

    uint8_t padding[3] = {0,0,0};
    int FD = open_stream("PduTests/pdu_quit_valid.pdu");
    uint8_t OP = read_uint8(FD);
    pdu_quit* pdu_mock = pdu_quit_deserialize(&FD);
    assert_nBytes(pdu_mock->padding, padding, 3
    , "PDU_QUIT: DESERIALIZE: padding test");

    pdu_quit_free(pdu_mock);
    close(FD);
    return true;
}

bool pdu_pleave_serialize_test () {
    uint8_t identityLength = 5;
    uint8_t* padding = "\0\0\0";
    uint32_t timeStamp = time(NULL);
    char* identity = "testa";
    pdu_pleave* pdu = pdu_pleave_create (identityLength, identity, timeStamp);
    uint8_t* buffer = pdu_pleave_serialize (pdu);

    assert_uint8 (buffer[0], OP_PLEAVE
        , "PDU_PLEAVE: SERIALIZE: OP-code test");

    assert_uint8 (buffer[1], identityLength
        , "PDU_PLEAVE: SERIALIZE: identity length test");

    assert_uint32 (parse_uint32(buffer+4), timeStamp
        , "PDU_PLEAVE: SERIALIZE: time stamp test");

    assert_nBytes (buffer+8, identity, identityLength
        , "PDU_PLEAVE: SERIALIZE: identity test");

    assert_nBytes(buffer+8+identityLength, padding, 3
        , "PDU_PLEAVE: SERIALIZE: identity padding");

    free (buffer);
    pdu_pleave_free (pdu);
    return true;
}

bool pdu_pleave_deserialize_test () {
    uint8_t identityLength = 6;
    uint8_t* identity = "testas";
    uint32_t timeStamp = 12;
    int FD = open_stream("PduTests/pdu_pleave_valid.pdu");
    uint8_t OP = read_uint8(FD);
    pdu_pleave* pdu_mock = pdu_pleave_deserialize(&FD);

    assert_uint8(*(pdu_mock->identityLength), identityLength
    , "PDU_PLEAVE: DESERIALIZE: identityLength test");

    assert_uint16(*(pdu_mock->padding_16b), 0
    , "PDU_PLEAVE: DESERIALIZE: padding 16b test");

    assert_uint32(*(pdu_mock->timeStamp), timeStamp
    , "PDU_PLEAVE: DESERIALIZE: timeStamp test");

    assert_nBytes(pdu_mock->identity, identity, identityLength
    , "PDU_PLEAVE: DESERIALIZE: identity test");

    pdu_pleave_free(pdu_mock);
    close(FD);
    return true;
}

bool pdu_pjoin_serialize_test () {
    uint8_t identityLength = 5;
    uint8_t* padding = "\0\0\0";
    uint32_t timeStamp = time(NULL);
    char* identity = "testa";
    pdu_pjoin* pdu = pdu_pjoin_create (identityLength, identity, timeStamp);
    uint8_t* buffer = pdu_pjoin_serialize (pdu);

    assert_uint8 (buffer[0], OP_PJOIN
        , "PDU_PJOIN: SERIALIZE: OP-code test");

    assert_uint8 (buffer[1], identityLength
        , "PDU_PJOIN: SERIALIZE: identity length test");

    assert_uint32 (parse_uint32(buffer+4), timeStamp
        , "PDU_PJOIN: SERIALIZE: time stamp test");

    assert_nBytes (buffer+8, identity, identityLength
        , "PDU_PJOIN: SERIALIZE: identity test");

    assert_nBytes(buffer+8+identityLength, padding, 3
        , "PDU_PJOIN: SERIALIZE: identity padding");


    free (buffer);
    pdu_pjoin_free (pdu);
    return true;
}

bool pdu_pjoin_deserialize_test() {
    uint8_t identityLength = 6;
    uint8_t* identity = "testas";
    uint32_t timeStamp = 12;
    int FD = open_stream("PduTests/pdu_pjoin_valid.pdu");
    uint8_t OP = read_uint8(FD);
    pdu_pjoin* pdu_mock = pdu_pjoin_deserialize(&FD);

    assert_uint8(*(pdu_mock->identityLength), identityLength
    , "PDU_PJOIN: DESERIALIZE: identityLength test");

    assert_uint16(*(pdu_mock->padding_16b), 0
    , "PDU_PJOIN: DESERIALIZE: padding 16b test");

    assert_uint32(*(pdu_mock->timeStamp), timeStamp
    , "PDU_PJOIN: DESERIALIZE: timeStamp test");

    assert_nBytes(pdu_mock->identity, identity, identityLength
    , "PDU_PJOIN: DESERIALIZE: identity test");

    pdu_pjoin_free(pdu_mock);
    close(FD);
    return true;
}

bool pdu_notreg_serialize_test () {
    uint16_t ID = 1337;
    pdu_notreg* pdu = pdu_notreg_create (ID);
    uint8_t* buffer = pdu_notreg_serialize (pdu);

    assert_uint8 (buffer[0], OP_NOTREG
        , "PDU_NOTREG: SERIALIZE: OP-code test");

    assert_uint8 (buffer[1], 0
        , "PDU_NOTREG: SERIALIZE: padding test");

    assert_uint16 (parse_uint16(buffer +2), ID
        , "PDU_NOTREG: SERIALIZE: ID test");

    free (buffer);
    pdu_notreg_free (pdu);
    return true;
}

bool pdu_notreg_deserialize_test () {
    uint16_t ID = 1337;

    int FD = open_stream("PduTests/pdu_notreg_valid.pdu");
    uint8_t OP = read_uint8(FD);
    pdu_notreg* pdu_mock = pdu_notreg_deserialize(&FD);

    assert_uint8(*(pdu_mock->padding), 0
    , "PDU_NOTREG: DESERIALIZE: pading test");

    assert_uint16(*(pdu_mock->ID), ID
    , "PDU_NOTREG: DESERIALIZE: ID test");

    pdu_notreg_free(pdu_mock);
    return true;
}

bool pdu_mess_serialize_test () {
    uint8_t identityLength = 13;
    uint16_t messageLength = 9;
    uint32_t timeStamp = 123;
    uint8_t* message = "testatest";
    uint8_t* messagePadding = "\0\0\0";
    uint8_t* identity = "test identity";
    uint8_t* identityPadding ="\0\0\0";
    uint8_t checksum =0;
    pdu_mess* pdu = pdu_mess_create (identityLength, checksum,  messageLength, timeStamp, message, identity);

    fprintf(stderr, "pdu_mess: \n");
    pdu_mess_print (pdu);

    uint8_t* buffer = pdu_mess_serialize (pdu);

    assert_uint8 (buffer[0], OP_MESS
        , "PDU_MESS: SERIALIZE: OP-code test");

    assert_uint8(buffer[1], 0
        , "PDU_MESS: SERIALIZE: padding 8b test");

    assert_uint8 (buffer[2], identityLength
        , "PDU_MESS: SERIALIZE: identity length test");

    assert_uint8 (buffer[3], checksum
        , "PDU_MESS: SERIALIZE: checksum test");

    assert_uint16 (parse_uint16(buffer+4), messageLength
        , "PDU_MESS: SERIALIZE: message length test");

    assert_uint16 (parse_uint16(buffer+6), 0
        , "PDU_MESS: SERIALIZE: padding 16b test");

    assert_uint32 (parse_uint32(buffer+8), timeStamp
        , "PDU_MESS: SERIALIZE: time stamp test");

    assert_nBytes (buffer+12, message, messageLength
        , "PDU_MESS: SERIALIZE: message test");

    assert_nBytes (buffer+12+messageLength, messagePadding, 3
        , "PDU_MESS: SERIALIZE: message padding test");

    int padding = calc_padding_length(messageLength);
    int index = (12+messageLength+padding);
    assert_nBytes (buffer+index, identity, identityLength
        , "PDU_MESS: SERIALIZE: client identity test");


    assert_nBytes(buffer+index+identityLength, identityPadding, 3
        , "PDU_MESS: SERIALIZE: identity padding test");

    free (buffer);
    pdu_mess_free(pdu);
    return true;
}

bool pdu_mess_deserialize_test () {
    uint8_t identityLength = 5;
    uint16_t messageLength = 11;
    uint32_t timeStamp = 12;
    uint8_t* message = "testmessage";
    uint8_t* identity = "testa";
    int FD = open_stream("PduTests/pdu_mess_valid.pdu");
    uint8_t OP = read_uint8(FD);
    pdu_mess* pdu_mock = pdu_mess_deserialize(&FD);

    assert_uint8(*(pdu_mock->padding_8b), 0
    , "PDU_MESS: DESERIALIZE: padding 8b test");

    assert_uint8(*(pdu_mock->identityLength), identityLength
    , "PDU_MESS: DESERIALIZE: identity length test");

    assert_uint8(*(pdu_mock->checksum), 13
    , "PDU_MESS: DESERIALIZE: checksum test");

    assert_uint16(*(pdu_mock->messageLength), messageLength
    , "PDU_MESS: DESERIALIZE: message length test");

    assert_uint16(*(pdu_mock->padding_16b), 0
    , "PDU_MESS: DESERIALIZE: padding 16b test");

    assert_uint32(*(pdu_mock->timeStamp), timeStamp
    , "PDU_MESS: DESERIALIZE: time stamp test");

    assert_nBytes(pdu_mock->message, message, 11
    , "PDU_MESS: DESERIALIZE: message test");

    assert_nBytes(pdu_mock->clientIdentity, identity, 5
    , "PDU_MESS: DESERIALIZE: client identity test");

    pdu_mess_free(pdu_mock);
    close(FD);
    return true;
}




bool pdu_slist_serialize_test () {

    uint16_t nrOfServers = 2;

    uint8_t** addresses = malloc (2*sizeof(uint8_t*));
    addresses[0] = malloc (4*sizeof(uint8_t));
    addresses[1] = malloc (4*sizeof(uint8_t));
    addresses[0][0] = 130;
    addresses[0][1] = 239;
    addresses[0][2] = 42;
    addresses[0][3] = 227;
    addresses[1][0] = 255;
    addresses[1][1] = 130;
    addresses[1][2] = 25;
    addresses[1][3] = 231;
    char* serverNames[] = {"hdjsahgsahaha","jeppjepp"};
    uint16_t ports[] = {1337, 55};
    uint8_t arrayOfNrOfClients[] = {5, 4};
    uint8_t serverNameLengths[] = {13, 8};
    pdu_slist* pdu_slist1 = pdu_slist_create(nrOfServers);
    pdu_slist_add_servers(pdu_slist1, addresses, ports, arrayOfNrOfClients,
                          serverNameLengths, serverNames);

    uint8_t * buffer = pdu_slist_serialize (pdu_slist1);
    // pdu_slist_free(pdu_slist1);

    assert_uint8(buffer[0], 4,
                 "PDU_SLIST: SERIALIZE: OP code test");

    assert_uint8(buffer[1], 0,
                 "PDU_SLIST: SERIALIZE: pad test");

    assert_uint16(parse_uint16(buffer + 2),  2,
                  "PDU_SLIST: SERIALIZE: nr of servers test");

    uint8_t* name1 = "hdjsahgsahaha";
    assert_uint32(parse_uint32(buffer + 4), htonl(*(uint32_t*) addresses[0]),
                  "PDU_SLIST: SERIALIZE: first address test");

    assert_uint16(parse_uint16(buffer + 8), 1337,
                  "PDU_SLIST: SERIALIZE: first port number test");

    assert_uint8(buffer[10], 5, "PDU_SLIST: SERIALIZE: first nr of clients test");

    assert_uint8(buffer[11], 13, "PDU_SLIST: SERIALIZE: first servername length test");

    assert_nBytes(buffer + 12, name1, 13, "PDU_SLIST: SERIALIZE: first servername test");

    uint8_t* name2 = "jeppjepp";

    assert_uint32(parse_uint32(buffer + 12+16), htonl(*(uint32_t*) addresses[1]),
                  "PDU_SLIST: SERIALIZE: second address test");

    assert_uint16(parse_uint16(buffer + 28+4), 55, "PDU_SLIST: SERIALIZE: second port number test");

    assert_uint8(buffer[28+6], 4, "PDU_SLIST: SERIALIZE: second nr of clients test");

    assert_uint8(buffer[28+7], 8, "PDU_SLIST: SERIALIZE: second servername length test");

    assert_nBytes(buffer + 28+8, name2, 8, "PDU_SLIST: SERIALIZE: second servername test");

    free(addresses[0]);
    free(addresses[1]);
    free(addresses);
    free(buffer);
    return true;
}

bool pdu_slist_deserialize_test () {


    uint8_t** addresses = malloc (2*sizeof(uint8_t*));
    addresses[0] = malloc (4*sizeof(uint8_t));
    addresses[1] = malloc (4*sizeof(uint8_t));
    addresses[0][0] = 130;
    addresses[0][1] = 239;
    addresses[0][2] = 42;
    addresses[0][3] = 227;
    addresses[1][0] = 255;
    addresses[1][1] = 130;
    addresses[1][2] = 25;
    addresses[1][3] = 231;
    //fprintf(stderr, "ref address 1: %u\n", *(uint32_t*)address1);
    uint8_t* name1 = "hdjsahgsahaha";
    //fprintf(stderr, "ref servname 1: %u\n", name1);

    //fprintf(stderr, "ref address 2: %u\n", *(uint32_t*)address2);
    uint8_t* name2 = "jeppjepp";
    //fprintf(stderr, "ref servname 2: %d\n", name2);

    int FD = open_stream ("PduTests/pdu_slist_valid.pdu");
    uint8_t OP = read_uint8(FD);
    pdu_slist* pdu_mock = pdu_slist_deserialize(&FD);
    //pdu_slist_print(pdu_mock);

    assert_uint8(*(pdu_mock -> padding), 0, "PDU_SLIST: DESERIALIZE: padding test");
    assert_uint16(*(pdu_mock -> nrOfServers), 2, "PDU_SLIST: DESERIALIZE: nr of servers test");

    assert_nBytes(pdu_mock -> servers[0] -> address, addresses[0], 4,
                  "PDU_SLIST:"
            " DESERIALIZE: address test one");
    assert_uint16(*(pdu_mock -> servers[0] -> port), 1337, "PDU_SLIST: DESERIALIZE: port test one");
    assert_uint8(*(pdu_mock -> servers[0] -> nrOfClients), 5, "PDU_SLIST: DESERIALIZE: nr of clients test one");
    assert_uint8(*(pdu_mock -> servers[0] -> serverNameLength), 13, "PDU_SLIST: DESERIALIZE: server name length test one");
    assert_uint32(*(pdu_mock -> servers[0] -> serverName), *(uint32_t*) name1, "PDU_SLIST: DESERIALIZE: server name test one");


    assert_nBytes(pdu_mock -> servers[1] -> address, addresses[1], 4,
                  "PDU_SLIST: DESERIALIZE: address test two");
    assert_uint16(*(pdu_mock -> servers[1] -> port), 55, "PDU_SLIST: DESERIALIZE: port test two");
    assert_uint8(*(pdu_mock -> servers[1] -> nrOfClients), 4, "PDU_SLIST: DESERIALIZE: nr of clients test two");
    assert_uint8(*(pdu_mock -> servers[1] -> serverNameLength), 8, "PDU_SLIST: DESERIALIZE: server name length test two");
    assert_uint32(*(pdu_mock -> servers[1] -> serverName), *(uint32_t*) name2, "PDU_SLIST: DESERIALIZE: server name test two");

    free(addresses[0]);
    free(addresses[1]);
    free(addresses);
    pdu_slist_free (pdu_mock);
    return true;
}
