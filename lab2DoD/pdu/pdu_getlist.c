#include "pdu.h"

pdu_getlist* pdu_getlist_create () {
    pdu_getlist* pdu = safe_calloc (sizeof (pdu_getlist));
    pdu -> def_pdu = pdu_create (OP_GETLIST, &pdu_getlist_serialize, &pdu_getlist_deserialize, 4);

    pdu -> padding = safe_calloc(3);

    return pdu;
}


void pdu_getlist_free (pdu_getlist* pdu) {
    pdu_free (pdu -> def_pdu);
    free (pdu -> padding);
    free (pdu);
}


void* pdu_getlist_serialize (void* pdu_to_serialize) {
    pdu_getlist* pdu = (pdu_getlist*) pdu_to_serialize;
    uint8_t* buffer = safe_calloc (4);

    serialize_uint8(buffer, pdu -> def_pdu -> op);
    serialize_nBytes(buffer+1, pdu -> padding, 3);

    return buffer;
}

bool pdu_getlist_validate_padding (uint8_t* padding) {
    for(int i = 0; i<3; i++ )
        if(padding[i] != 0){
            fprintf (stderr, "Padding missing\n");
            return false;
        }
    return true;
}

void* pdu_getlist_deserialize (void* byte_stream) {
    int FD = *(int*)byte_stream;

    uint8_t* padding = read_nBytes(FD, 3);
    if (!pdu_getlist_validate_padding (padding)){
        validation_failed("PDU_GETLIST");
        return NULL;
    }
    free(padding);

    return pdu_getlist_create ();
}
