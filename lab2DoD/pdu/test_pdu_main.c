
#include "pdu_test.h"


int main () {


    if(pdu_reg_test())
        fprintf(stderr, "PDU_REG test completed!\n");

    if(pdu_alive_test())
        fprintf(stderr, "PDU_ALIVE test completed!\n");

    if(pdu_ack_test())
        fprintf(stderr, "PDU_ACK test completed!\n");

    if(pdu_notreg_test())
        fprintf(stderr, "PDU_NOTREG test completed!\n");

    if(pdu_getlist_test())
        fprintf(stderr, "PDU_GETLIST test completed!\n");

    if(pdu_join_test())
        fprintf(stderr, "PDU_JOIN test completed!\n");

    if(pdu_participants_test())
        fprintf(stderr, "PDU_PARTICIPANTS test completed!\n");

    if(pdu_quit_test())
        fprintf(stderr, "PDU_QUIT test completed!\n");

    if(pdu_pjoin_test())
        fprintf(stderr, "PDU_PJOIN test completed!\n");

    if(pdu_pleave_test())
        fprintf(stderr, "PDU_PLEAVE test completed!\n");

    if(pdu_mess_test())
        fprintf(stderr, "PDU_MESS test completed!\n");

    if(pdu_slist_test())
        fprintf(stderr, "PDU_SLIST test completed!\n");

    return 0;
}
