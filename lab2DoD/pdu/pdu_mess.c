#include "pdu.h"

int pdu_mess_calc_pduSize(uint16_t messageLength, uint8_t identityLength){
    int messagePadding = calc_padding_length (messageLength);
    int identityPadding = calc_padding_length(identityLength);
    return 12 + messageLength + messagePadding + identityLength + identityPadding;
}

pdu_mess* pdu_mess_init(uint16_t messageLength, uint8_t clientLength){
    pdu_mess* pdu = safe_calloc (sizeof (pdu_mess));
    pdu -> def_pdu = pdu_create (OP_MESS,
        &pdu_mess_serialize, &pdu_mess_deserialize,
        pdu_mess_calc_pduSize(messageLength, clientLength));

    pdu->padding_8b = safe_calloc(1);
    pdu->identityLength = safe_calloc(1);
    pdu->checksum = safe_calloc(1);

    pdu->messageLength = safe_calloc(2);
    pdu->padding_16b = safe_calloc(2);

    pdu->timeStamp = safe_calloc(4);
    pdu->message = safe_calloc(messageLength + 1);
    pdu->clientIdentity = safe_calloc(clientLength + 1);
}

pdu_mess* pdu_mess_create (uint8_t clientLength, uint8_t checksum, uint16_t messageLength
    , uint32_t timeStamp, uint8_t* message, uint8_t* clientIdentity) {

    pdu_mess* pdu = pdu_mess_init(messageLength, clientLength);

    set_uint8(pdu->identityLength, &clientLength);
    set_uint8(pdu->checksum, &checksum);
    set_uint16(pdu->messageLength, &messageLength);
    set_uint32(pdu->timeStamp, &timeStamp);
    set_nBytes(pdu->message, message, messageLength);
    set_nBytes(pdu->clientIdentity, clientIdentity, clientLength);

    return pdu;
}

void pdu_mess_printDate(pdu_mess* pdu){
    time_t time = *(pdu->timeStamp);
    struct tm* date = localtime(&time);
    fprintf(stderr, "    Time:%.2d:%0.2d:%0.2d\n",date->tm_hour, date->tm_min, date->tm_sec);
}



uint8_t* pdu_mess_print (pdu_mess* pdu) {
    printf("\n");
    pdu_mess_printDate(pdu);
    printf("    ID:%s\n", (char*)pdu->clientIdentity);
    printf("    Message:%s\n\n", (char*)pdu->message);
}

void pdu_mess_free (pdu_mess* pdu) {
    pdu_free (pdu -> def_pdu);
    free (pdu -> padding_8b);
    free (pdu -> identityLength);
    free (pdu -> checksum);
    free (pdu -> messageLength);
    free (pdu -> padding_16b);
    free (pdu -> timeStamp);
    free (pdu -> message);
    free (pdu -> clientIdentity);
    free (pdu);
}


void* pdu_mess_serialize (void* pdu_to_serialize) {

    pdu_mess* pdu = (pdu_mess*) pdu_to_serialize;
    int messageLength = *(pdu -> messageLength);
    int identityLength = *(pdu -> identityLength);
    int messagePadding = calc_padding_length(messageLength);

    uint8_t* buffer = safe_calloc(*(pdu->def_pdu->size));

    serialize_uint8(buffer, pdu -> def_pdu -> op);
    serialize_uint8(buffer+1, pdu -> padding_8b);
    serialize_uint8(buffer+2, pdu -> identityLength);
    serialize_uint8(buffer+3, pdu -> checksum);
    serialize_uint16(buffer+4, pdu -> messageLength);

    serialize_uint16(buffer+6, pdu -> padding_16b);
    serialize_uint32(buffer+8, pdu -> timeStamp);
    serialize_nBytes(buffer+12, pdu -> message, messageLength);
    serialize_nBytes(buffer+12+messageLength+messagePadding, pdu -> clientIdentity, identityLength);

    return buffer;
}


bool pdu_mess_validate_padding_8b(uint8_t padding){
    if(padding != 0){
        fprintf(stderr, "missing 8b padding\n");
        return false;
    }
    return true;
}

bool pdu_mess_validate_padding_16b(uint16_t padding){
    if(padding != 0){
        fprintf(stderr, "missing 16b padding\n");
        return false;
    }
    return true;
}

bool pdu_mess_validate_length(uint8_t* string, uint16_t length, char* stringName){
    for(uint16_t i=0; i<length; i++){
        if(string[i] == 0){
            fprintf(stderr, "%s length not equal to given length: expected:%d found:%d\n", stringName, length, i );
            return false;
        }
    }
    return true;
}

uint8_t pdu_mess_calc_pduSum(pdu_mess* pdu){

    int sum = 0;
    if((sum += *(pdu->def_pdu->op)) > 255)
        sum -= 255;

    if((sum += *(pdu->padding_8b)) > 255)
        sum -= 255;

    if((sum += *(pdu->identityLength)) > 255)
        sum -= 255;

    if((sum += *(pdu->checksum)) > 255)
        sum -= 255;

    for(int i=0; i<2;i++)
        if((sum += ((uint8_t*)(pdu->messageLength))[i]) > 255 )
            sum -= 255;

    for(int i=0; i<2;i++)
        if((sum += ((uint8_t*)(pdu->padding_16b))[i]) > 255 )
            sum -= 255;

    for(int i=0; i<4;i++)
        if((sum += ((uint8_t*)(pdu->timeStamp))[i]) > 255 )
            sum -= 255;

    for(int i=0; i<*(pdu->messageLength);i++)
        if((sum += ((uint8_t*)(pdu->message))[i]) > 255 )
            sum -= 255;

    for(int i=0; i<*(pdu->identityLength);i++)
        if((sum += ((uint8_t*)(pdu->clientIdentity))[i]) > 255 )
            sum -= 255;

    return sum;
}

bool pdu_mess_validate_checksum(pdu_mess* pdu, uint8_t checksum){
    int sum = pdu_mess_calc_pduSum(pdu);
    if((sum + checksum) != 255){
        fprintf(stderr, "incorrect checksum: %d + %d = %d\n",checksum, sum, checksum + sum);
        return false;
    }
    return true;
}

bool pdu_mess_validate_reads(uint8_t checksum, uint16_t messageLength, uint32_t timeStamp,
                             uint8_t* message, uint8_t* clientIdentity){
    if(messageLength == 0 || message == NULL || clientIdentity == NULL)
        return false;
    return true;

}

void* pdu_mess_deserialize (void* stream) {
    int FD = *((int*)stream);

    uint8_t padding_8b = read_uint8(FD);
    uint8_t identityLength = read_uint8(FD);
    uint8_t checksum = read_uint8(FD);
    uint16_t messageLength = read_uint16(FD);
    uint16_t padding_16b = read_uint16(FD);
    uint32_t timeStamp = read_uint32(FD);
    uint8_t* message = read_nBytes(FD, messageLength + calc_padding_length(messageLength));
    uint8_t* clientIdentity = read_nBytes(FD, identityLength + calc_padding_length(identityLength));

    pdu_mess* mockPdu = pdu_mess_create (identityLength, 0, messageLength, timeStamp, message, clientIdentity);
    if(!pdu_mess_validate_padding_8b(padding_8b)
        || !pdu_mess_validate_padding_16b(padding_16b)
        || !pdu_mess_validate_checksum(mockPdu, checksum)
        || !pdu_mess_validate_reads(checksum, messageLength, timeStamp, message, clientIdentity)){
            validation_failed("PDU_MESS");
            return NULL;
    }

    pdu_mess* pdu;
    if(!identityLength){
        identityLength = 29;
        clientIdentity = strdup("|SERVER|SERVER|SERVER|SERVER|");
        pdu = pdu_mess_create(identityLength, checksum, messageLength, timeStamp, message, clientIdentity);
    }else{
        pdu = pdu_mess_create(identityLength, checksum, messageLength, timeStamp, message, clientIdentity);
    }

    pdu_mess_free (mockPdu);
    free (message);
    free (clientIdentity);
    return pdu;
}

void pdu_mess_updateSize(pdu_mess* pdu){
    pdu_setSize(pdu, pdu_mess_calc_pduSize(*(pdu->messageLength), *(pdu->identityLength)));
}

void pdu_mess_setIdentity(pdu_mess* pdu, uint8_t* identity, uint8_t length){

    set_uint8(pdu->identityLength, &length);
    set_nBytes(pdu->clientIdentity, identity, length);
}


void pdu_mess_setChecksum(pdu_mess* pdu, uint8_t checksum){
    set_uint8(pdu->checksum, &checksum);
}

void pdu_mess_setTimestamp(pdu_mess* pdu, uint32_t timeStamp){
    set_uint32(pdu->timeStamp, &timeStamp);
}
