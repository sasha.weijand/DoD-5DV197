#ifndef LAB2DOD_PDU_H
#define LAB2DOD_PDU_H


#include "pdu_util.h"


#define OP_REG 0
#define OP_ACK 1
#define OP_ALIVE 2
#define OP_GETLIST 3
#define OP_SLIST 4
#define OP_MESS 10
#define OP_QUIT 11
#define OP_JOIN 12
#define OP_PJOIN 16
#define OP_PLEAVE 17
#define OP_PARTICIPANTS 19
#define OP_NOTREG 100

typedef void* serialize(void*);
typedef void* deserialize(void*);

typedef struct pdu {
    uint8_t* op;
    int* size;
    serialize* ser;
    deserialize* deSer;
}pdu;

typedef struct pdu_reg {
    pdu* def_pdu;
    uint8_t* serverNameLength;
    uint16_t* port;
    uint32_t* serverName;
} pdu_reg;

typedef struct pdu_quit {
    pdu* def_pdu;
    uint8_t* padding;
} pdu_quit;

typedef struct pdu_getlist {
    pdu* def_pdu;
    uint8_t* padding;
} pdu_getlist;

typedef struct pdu_pleave {
    pdu* def_pdu;
    uint8_t* identityLength;
    uint16_t* padding_16b;
    uint32_t* timeStamp;
    uint32_t* identity;
} pdu_pleave;

typedef struct pdu_pjoin {
    pdu* def_pdu;
    uint8_t* identityLength;
    uint16_t* padding_16b;
    uint32_t* timeStamp;
    uint32_t* identity;
} pdu_pjoin;

typedef struct pdu_participants {
    pdu* def_pdu;
    uint8_t* numberOfIdentities;
    uint16_t* length;
    uint32_t* identities;
} pdu_participants;

typedef struct pdu_ack {
    pdu* def_pdu;
    uint8_t* padding;
    uint16_t* ID;
} pdu_ack;

typedef struct pdu_notreg {
    pdu* def_pdu;
    uint8_t* padding;
    uint16_t* ID;
} pdu_notreg;

typedef struct pdu_mess {
    pdu* def_pdu;
    uint8_t* padding_8b;
    uint8_t* identityLength;
    uint8_t* checksum;
    uint16_t* messageLength;
    uint16_t* padding_16b;
    uint32_t* timeStamp;
    uint32_t* message;
    uint32_t* clientIdentity;
} pdu_mess;


typedef struct pdu_join {
    pdu* def_pdu;
    uint8_t* identityLength;
    uint16_t* padding_16b;
    uint32_t* identity;
} pdu_join;

typedef struct pdu_alive {
    pdu* def_pdu;
    uint8_t* nrOfClients;
    uint16_t* ID;

} pdu_alive;

typedef struct server {
    uint32_t* address;
    uint16_t*  port;
    uint8_t* nrOfClients;
    uint8_t* serverNameLength;
    uint32_t* serverName;
} server;


typedef struct pdu_slist {
    pdu* def_pdu;
    uint8_t* padding;
    uint16_t* nrOfServers;
    server** servers;
} pdu_slist;

void pdu_setSize(void* tmpPdu, int size);
int pdu_getSize(void* pduToWrite);
int pdu_getOP(void* pdu);
void pdu_any_free (pdu* pduToFree);

uint8_t pdu_participants_getNumberOfParticipants(pdu_participants* pdu);
uint8_t* pdu_participants_getParticipants(pdu_participants* pdu);

uint16_t pdu_ack_getID(pdu_ack* pdu);
uint16_t pdu_alive_getID(pdu_alive* pdu);
uint8_t* pdu_join_getIdentity(pdu_join* pdu);
uint8_t* pdu_pjoin_get_identity (pdu_pjoin* pdu);
uint8_t* pdu_pleave_get_identity (pdu_pleave* pdu);

void pdu_mess_updateSize(pdu_mess* pdu);
void pdu_mess_setIdentity (pdu_mess* pdu, uint8_t* identity, uint8_t length);
void pdu_mess_setChecksum (pdu_mess* pdu, uint8_t checksum);
void pdu_mess_setTimestamp (pdu_mess* pdu, uint32_t timestamp);
uint8_t pdu_mess_calc_pduSum (pdu_mess* pdu);
uint8_t* pdu_mess_print (pdu_mess* pdu);

void pdu_slist_print (pdu_slist* pdu);
char* pdu_slist_get_port (pdu_slist* slist, int num);
char* pdu_slist_get_server_ip_address (pdu_slist* slist, int num);

pdu* pdu_create(uint8_t op, serialize* ser, deserialize* deSer, int size);
pdu_reg* pdu_reg_create (uint8_t serverNameLength, uint8_t* serverName, uint16_t port);
pdu_quit* pdu_quit_create ();
pdu_getlist* pdu_getlist_create ();
pdu_pleave* pdu_pleave_create (uint8_t identityLength, uint8_t* identity, uint32_t timeStamp);
pdu_pjoin* pdu_pjoin_create (uint8_t identityLength, uint8_t* identity, uint32_t timeStamp);
pdu_participants* pdu_participants_create (uint8_t numberOfIdentities, uint16_t length, uint8_t* identities);
pdu_ack* pdu_ack_create (uint16_t port);
pdu_notreg* pdu_notreg_create (uint16_t port);
pdu_mess* pdu_mess_create (uint8_t identityLength, uint8_t checksum, uint16_t messageLength
    , uint32_t timeStamp, uint8_t* message, uint8_t* clientIdentity);
pdu_join* pdu_join_create (uint8_t identityLength, uint8_t* identity);
pdu_alive* pdu_alive_create (uint8_t nrClients, uint16_t ID);
pdu_slist* pdu_slist_create (uint16_t nrServers);
void pdu_slist_add_servers (pdu_slist* pdu, uint8_t** addresses, uint16_t* ports,
                            uint8_t* arrayOfNrOfClients, uint8_t* serverNameLengths, char** serverNames);

uint8_t* pdu_serialize(void* pduToSerialize);
void* pdu_reg_serialize (void* pduToSerialize);
void* pdu_quit_serialize (void* pdu_to_serialize);
void* pdu_getlist_serialize (void* pdu_to_serialize);
void* pdu_pleave_serialize (void* pduToSerialize);
void* pdu_pjoin_serialize (void* pduToSerialize);
void* pdu_participants_serialize (void* pduToSerialize);
void* pdu_ack_serialize (void* pdu_to_serialize);
void* pdu_notreg_serialize (void* pdu_to_serialize);
void* pdu_mess_serialize (void* pduToSerialize);
void* pdu_join_serialize (void* pduToSerialize);
void* pdu_alive_serialize (void* pduToSerialize);
void* pdu_slist_serialize (void* pduToSerialize);

void* pdu_deserialize(int FD);
void* pdu_reg_deserialize (void* byte_stream);
void* pdu_quit_deserialize (void* byte_stream);
void* pdu_getlist_deserialize (void* byte_stream);
void* pdu_pleave_deserialize (void* byte_stream);
void* pdu_pjoin_deserialize (void* byte_stream);
void* pdu_participants_deserialize (void* byte_stream);
void* pdu_ack_deserialize (void* byte_stream);
void* pdu_notreg_deserialize (void* byte_stream);
void* pdu_mess_deserialize (void* stream);
void* pdu_join_deserialize (void* byte_stream);
void* pdu_alive_deserialize (void* byte_stream);
void* pdu_slist_deserialize (void* byte_stream);

void pdu_free(pdu* pdu);
void pdu_reg_free (pdu_reg* pdu);
void pdu_quit_free (pdu_quit* pdu);
void pdu_getlist_free (pdu_getlist* pdu);
void pdu_pleave_free (pdu_pleave* pdu);
void pdu_pjoin_free (pdu_pjoin* pdu);
void pdu_participants_free (pdu_participants* pdu);
void pdu_ack_free (pdu_ack* pdu);
void pdu_notreg_free (pdu_notreg* pdu);
void pdu_mess_free (pdu_mess* pdu);
void pdu_join_free (pdu_join* pdu);
void pdu_alive_free (pdu_alive* pdu);
void pdu_slist_free (pdu_slist* pdu);


#endif //LAB2DOD_PDU_H
