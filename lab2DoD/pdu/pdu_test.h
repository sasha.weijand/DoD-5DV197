#include "pdu.h"

bool pdu_ack_test();
bool pdu_alive_test();
bool pdu_getlist_test();
bool pdu_join_test();
bool pdu_mess_test();
bool pdu_notreg_test();
bool pdu_participants_test();
bool pdu_pjoin_test();
bool pdu_pleave_test();
bool pdu_reg_test();
bool pdu_slist_test();
bool pdu_quit_test();

bool pdu_reg_serialize_test ();
bool pdu_reg_deserialize_test();

bool pdu_participants_serialize_test ();
bool pdu_participants_deserialize_test ();

bool pdu_join_serialize_test ();
bool pdu_join_deserialize_test ();

bool pdu_alive_serialize_test ();
bool pdu_alive_deserialize_test ();

bool pdu_ack_serialize_test ();
bool pdu_ack_deserialize_test () ;

bool pdu_quit_serialize_test ();
bool pdu_quit_deserialize_test () ;

bool pdu_getlist_serialize_test ();
bool pdu_getlist_deserialize_test () ;

bool pdu_pjoin_serialize_test ();
bool pdu_pjoin_deserialize_test () ;

bool pdu_pleave_serialize_test ();
bool pdu_pleave_deserialize_test () ;

bool pdu_notreg_serialize_test ();
bool pdu_notreg_deserialize_test ();

bool pdu_mess_serialize_test ();
bool pdu_mess_deserialize_test () ;

bool pdu_slist_serialize_test ();
bool pdu_slist_deserialize_test ();