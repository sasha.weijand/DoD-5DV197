#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE 700
#endif
#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include <malloc.h>
#include <memory.h>
#include <time.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netdb.h>
#include <net/if.h>
#include <netinet/in.h>
#include <err.h>


void* safe_calloc (int size);

int calc_padding_length(int length);

uint16_t parse_uint16(uint8_t* buffer);

uint32_t parse_uint32 (uint8_t* buffer);

uint8_t* parse_nBytes (uint8_t* byte_stream, int bytes);

void set_uint8(uint8_t* destination, uint8_t* source);

void set_uint16(uint16_t* destination, uint16_t* source);

void set_uint32(uint32_t* destination, uint32_t* source);

void set_nBytes(void* destination, void* source, uint16_t bytes);

uint8_t* read_nBytes(int FD, int bytes);

uint8_t read_uint8(int FD);

uint16_t read_uint16(int FD);

uint32_t read_uint32(int FD);

void serialize_uint8(uint8_t* destination, uint8_t* toSerialize);

void serialize_uint16(uint8_t* destination, uint16_t* toSerialize);

void serialize_uint32(uint8_t* destination, uint32_t* toSerialize);

void serialize_nBytes(void* destination, void* toSerialize, uint16_t bytes);

void validation_failed(char* pduType);

int open_stream(char* filepath);

bool assert_uint8 (uint8_t found, uint8_t expected, char* testname);

bool assert_uint16 (uint16_t found, uint16_t expected, char* testname);

bool assert_uint32 (uint32_t found, uint32_t expected, char* testname);

bool assert_nBytes(void* found, void* expected, uint16_t length, char* testname);
