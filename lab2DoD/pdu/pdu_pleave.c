#include "pdu.h"

int pdu_pleave_calc_pduSize(uint8_t identityLength){
    int padding_16bLength = calc_padding_length(identityLength);

    return 8 + identityLength + padding_16bLength;
}

pdu_pleave* pdu_pleave_init(uint8_t identityLength){
    pdu_pleave* pdu = safe_calloc (sizeof (pdu_pleave));
    pdu -> def_pdu = pdu_create (OP_PLEAVE,
        &pdu_pleave_serialize, &pdu_pleave_deserialize,
        pdu_pleave_calc_pduSize(identityLength));

    pdu -> identityLength = safe_calloc(1);
    pdu -> padding_16b = safe_calloc(2);
    pdu -> identity = safe_calloc(identityLength + 1);
    pdu -> timeStamp = safe_calloc(4);

    return pdu;
}

pdu_pleave* pdu_pleave_create (uint8_t identityLength, uint8_t* identity, uint32_t timeStamp) {
    pdu_pleave* pdu = pdu_pleave_init(identityLength);

    set_uint8(pdu -> identityLength, &identityLength);
    set_nBytes(pdu -> identity, identity, identityLength);
    set_uint32(pdu -> timeStamp, &timeStamp);

    return pdu;
}


uint8_t* pdu_pleave_get_identity (pdu_pleave* pdu) {
    return (uint8_t*) pdu->identity;
}

void pdu_pleave_free (pdu_pleave* pdu) {
    pdu_free (pdu -> def_pdu);
    free (pdu -> identity);
    free(pdu -> timeStamp);
    free (pdu -> identityLength);
    free (pdu -> padding_16b);
    free (pdu);
}



void* pdu_pleave_serialize (void* pdu_to_serialize) {
    pdu_pleave* pdu = (pdu_pleave*) pdu_to_serialize;
    uint8_t* buffer = safe_calloc(*(pdu->def_pdu->size));

    serialize_uint8(buffer, pdu -> def_pdu -> op);
    serialize_uint8(buffer+1, pdu -> identityLength);
    serialize_uint16(buffer+2, pdu -> padding_16b);
    serialize_uint32(buffer+4, pdu -> timeStamp);
    serialize_nBytes(buffer+8, pdu -> identity, *(pdu -> identityLength));

    return buffer;
}

bool pdu_pleave_validate_identityLength(uint8_t* identity, uint8_t length){
    for(int i = 0; i<length; i++){
        if(identity[i] == 0){
            fprintf(stderr, "identity length not equal to given length: found:%d expected:%d\n", i, length);
            return false;
        }
    }
    return true;
}

bool pdu_pleave_validate_padding_16b(uint8_t padding_16b){
    if(padding_16b != 0){
        fprintf(stderr, "padding_16b missing\n");
        return false;
    }
    return true;
}

bool pdu_pleave_validate(uint8_t* identity, uint8_t identityLength, uint8_t padding_16b){
    pdu_pleave_validate_identityLength(identity, identityLength);
    pdu_pleave_validate_padding_16b(padding_16b);
}


void* pdu_pleave_deserialize (void* stream) {
    int FD  = *(int*)stream;

    uint8_t identityLength = read_uint8(FD);
    uint8_t padding_16b = read_uint16(FD);
    uint32_t timeStamp = read_uint32(FD);
    uint8_t* identity = read_nBytes(FD, identityLength + calc_padding_length(identityLength) );

    if(!pdu_pleave_validate(identity, identityLength, padding_16b)){
        validation_failed("PDU_PLEAVE");
        return NULL;
    }

    pdu_pleave* pdu = pdu_pleave_create (identityLength, identity, timeStamp);
    free (identity);
    return pdu;

}
