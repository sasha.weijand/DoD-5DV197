#include "pdu.h"


pdu_notreg* pdu_notreg_init(){
    pdu_notreg* pdu = safe_calloc (sizeof (pdu_notreg));
    pdu -> def_pdu = pdu_create (OP_NOTREG, &pdu_notreg_serialize, &pdu_notreg_deserialize, 4);

    pdu -> padding = safe_calloc(1);
    pdu -> ID = safe_calloc (2);

    return pdu;
}
pdu_notreg* pdu_notreg_create (uint16_t ID) {
    pdu_notreg* pdu = pdu_notreg_init();


    set_uint16(pdu -> ID, &ID);
    return pdu;
}


void pdu_notreg_free (pdu_notreg* pdu) {
    pdu_free (pdu -> def_pdu);
    free (pdu -> ID);
    free (pdu -> padding);
    free (pdu);
}


void pdu_notreg_serialize_op(uint8_t* buffer, pdu_notreg* pdu){
    memcpy (&buffer[0], pdu -> def_pdu -> op, 1);
}

void pdu_notreg_serialize_ID(uint8_t* buffer, pdu_notreg* pdu){
    *(pdu -> ID) = htons(*(pdu -> ID));
    memcpy (&buffer[2], pdu -> ID, 2);
}

void* pdu_notreg_serialize (void* pdu_to_serialize) {
    pdu_notreg* pdu = (pdu_notreg*) pdu_to_serialize;
    uint8_t* buffer = safe_calloc (4);

    serialize_uint8(buffer, pdu -> def_pdu -> op);
    serialize_uint8(buffer+1, pdu -> padding);
    serialize_uint16(buffer+2, pdu -> ID);

    return buffer;
}

bool pdu_notreg_validate_padding (uint8_t padding) {
    return padding == 0;
}


void* pdu_notreg_deserialize (void* stream) {
    int FD = *(int*)stream;

    uint8_t padding = read_uint8(FD);
    uint16_t ID = read_uint16 (FD);

    if(!pdu_notreg_validate_padding(padding)){
        validation_failed("PDU_NOTREG");
        return NULL;
    }

    return pdu_notreg_create (ID);
}
