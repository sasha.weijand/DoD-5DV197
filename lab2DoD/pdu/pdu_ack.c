#include "pdu.h"


pdu_ack* pdu_ack_init(){
    pdu_ack* pdu = safe_calloc (sizeof (pdu_ack));

    pdu -> def_pdu = pdu_create (OP_ACK, &pdu_ack_serialize, &pdu_ack_deserialize, 4);
    pdu -> padding = safe_calloc(1);
    pdu -> ID = safe_calloc (2);

    return pdu;
}

pdu_ack* pdu_ack_create (uint16_t ID) {
    pdu_ack* pdu = pdu_ack_init();

    set_uint16(pdu -> ID, &ID);
    return pdu;
}


void pdu_ack_free (pdu_ack* pdu) {
    pdu_free (pdu -> def_pdu);
    free (pdu -> ID);
    free(pdu -> padding);
    free (pdu);
}


void* pdu_ack_serialize (void* pdu_to_serialize) {
    pdu_ack* pdu = (pdu_ack*) pdu_to_serialize;
    uint8_t* buffer = safe_calloc (4);

    serialize_uint8(buffer, pdu -> def_pdu -> op);
    serialize_uint8(buffer+1, pdu -> padding);
    serialize_uint16(buffer+2, pdu -> ID);

    return buffer;
}

bool pdu_ack_validate_padding (uint8_t padding) {
    if(padding != 0){
        fprintf(stderr, "padding missing\n");
        return false;
    }
    return true;
}



void* pdu_ack_deserialize (void* stream) {
    int FD = *(int*)stream;

    uint8_t padding = read_uint8(FD);
    uint16_t ID = read_uint16 (FD);

    if(!pdu_ack_validate_padding(padding)){
        validation_failed("PDU_ACK");
        return NULL;
    }

    return pdu_ack_create (ID);
}

uint16_t pdu_ack_getID(pdu_ack* pdu){
    return *(pdu->ID);
}
