#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE
#endif


#ifndef _NODE
#define _NODE

#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/poll.h>
#include <sys/time.h>
#include <stdint.h>
#include <signal.h>
#include <sys/types.h>




#include "../pdu/pdu.h"
#include "list/thread_list_lib.h"

bool online;
bool shouldQuit;
bool registered;
bool rdyForInput;
pthread_mutex_t clientsLock;

typedef struct host_info {
    char* name;
    char* port;
}host_info;

typedef struct node_info {
    list* messages;
    host_info* connectInfo;
    host_info* ownInfo;
}node_info;


typedef struct server_info {
    node_info* nodeInfo;
    uint16_t* ID;
    char* name;
    list* clients;
    int listenerSocket;
    int nameserverSocket;
}server_info;

typedef struct client_info {
    node_info* nodeInfo;
    char* identity;
    int clientsocket;
    int serversocket;
}client_info;

typedef struct message_info {
    void* pduToSend;
    int clientsocket;
}message_info;

/*
 * This is the signal handling function,
 * which is called automatically when the signal SIGINT is given.
 */
void SIG_handler(int sig);

/*
 * Initiates the sigaction struct with a
 * given signal handler (see above), and some other options.
 */
struct sigaction* setSignalHandler(void(*handler)(int));


void node_init_clients_lock ();

void node_destroy_clients_lock ();

char* node_getOwnHostName();

bool node_is_portNumber(char* port);

host_info* node_create_host_info(char* hostName, char* port);

node_info* node_create_node_info(host_info* ownInfo, host_info* toConnect);

server_info* node_create_server_info(node_info* nodeInfo, char* name, int nameserverSocket);

client_info* node_create_client_info(node_info* nodeInfo, char* identity);

void host_info_free(host_info* host);

void node_info_free (node_info* node);

void client_info_free (client_info* client);

void server_info_free (server_info* server);


void message_info_free (message_info* message);

list* node_getMessageQueue(node_info* node);

void node_server_info_setID(server_info* server, uint16_t ID);

uint16_t node_server_info_getID(server_info* server);

char* node_server_info_getServername(server_info* server);

char* node_server_info_getPort(server_info* server);

list* node_server_info_getClients(server_info* server);

int node_server_info_getNameserverSocket(server_info* server);

int node_server_info_getNumberOfClients(server_info* server);

void node_client_info_setServersocket(client_info* client, int serversocket);

int node_client_info_getServersocket(client_info* client);

void node_client_info_setSocket(client_info* client, int socket);

int node_client_info_getSocket(client_info* client);

void node_client_info_setIdentity(client_info* client, char* identity);

char* node_client_info_getIdentity(client_info* client);

message_info* node_message_info_create(void* pduToSend, int clientsocket);

#endif
