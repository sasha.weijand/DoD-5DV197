#include"node.h"

void SIG_handler(int sig){
	if (sig == SIGINT)
        shouldQuit = true;
}

struct sigaction* setSignalHandler(void(*handler)(int)){
    struct sigaction* sigac = safe_calloc(sizeof(struct sigaction));
    sigac->sa_handler = handler;
	sigac->sa_flags = SA_RESTART;
	sigaction(SIGINT, sigac, NULL);

    return sigac;
}

char* node_getOwnHostName() {
    char tmpName[255];
    memset(tmpName, 0, 255);

    if (gethostname(tmpName, 255) != 0) {
        fprintf(stderr, "Could not get own hostname\n");
        exit(EXIT_FAILURE);
    }

    char* name = safe_calloc(strlen(tmpName)+1);
    strcpy(name, tmpName);
    return name;
}

bool node_is_portNumber(char* port){
    int length = 0;
    while(port[length] != 0)
        length++;

    for(int i = 0; i < length; i++)
        if(isdigit(port[i]) == 0)
            return false;
    return true;
}


void node_init_clients_lock () {
    if (pthread_mutex_init (&clientsLock, NULL) != 0) {
        fprintf (stderr, "Error, initializing \n");
        exit (EXIT_FAILURE);
    }
}

void node_destroy_clients_lock () {
    if (pthread_mutex_destroy (&clientsLock) != 0) {
        fprintf (stderr, "%s\n", strerror (errno));
        exit (EXIT_FAILURE);
    }
}

host_info* node_create_host_info(char* hostName, char* port){
    host_info* info = safe_calloc(sizeof(host_info));
    info->name = safe_calloc(strlen(hostName)+1);
    info->port = safe_calloc(strlen(port)+1);
    memcpy(info->name, hostName, strlen(hostName));
    memcpy(info->port, port, strlen(port));

    return info;
}

node_info* node_create_node_info(host_info* ownInfo, host_info* toConnect){
    node_info* node = safe_calloc(sizeof(node_info));
    node->messages = list_empty();

    node->connectInfo = toConnect;
    node->ownInfo = ownInfo;

    return node;
}

void node_info_free (node_info* node) {
    list_free (node->messages);
    host_info_free(node->connectInfo);
    host_info_free(node->ownInfo);
    free (node);
}

void client_info_free (client_info* client) {
    node_info_free (client->nodeInfo);
    free(client->identity);
    free (client);
}

void server_info_free (server_info* server) {
    node_info_free (server->nodeInfo);
    free (server->ID);
    list_free (server->clients);
    free (server);
}

void host_info_free(host_info* host){
    free(host->name);
    free(host->port);
    free(host);
}

void message_info_free (message_info* message) {
    pdu_any_free (message->pduToSend);
    free (message);
}

void node_server_info_setID(server_info* server, uint16_t ID){
    *(server->ID) = ID;
}

uint16_t node_server_info_getID(server_info* server){
    return *(server->ID);
}

char* node_server_info_getServername(server_info* server){
    return server->name;
}

char* node_server_info_getPort(server_info* server){
    return server->nodeInfo->ownInfo->port;
}

list* node_server_info_getClients(server_info* server){
    return server->clients;
}

int node_server_info_getNameserverSocket(server_info* server){
    return server->nameserverSocket;
}

int node_server_info_getNumberOfClients(server_info* server){
    return list_size_t(server->clients);
}

list* node_getMessageQueue(node_info* node){
    return node->messages;
}

server_info* node_create_server_info(node_info* nodeInfo, char* name, int nameserverSocket){
    server_info* server = safe_calloc(sizeof(server_info));
    server->nodeInfo = nodeInfo;
    server->name = name;
    server->ID = safe_calloc(2);
    server->clients = list_empty();
    server->nameserverSocket = nameserverSocket;

    return server;
}

client_info* node_create_client_info(node_info* nodeInfo, char* identity){
    client_info* client = safe_calloc(sizeof(client_info));

    client->nodeInfo = nodeInfo;
    client->identity = safe_calloc(strlen(identity) +1 );
    memcpy(client->identity, identity, strlen(identity));

    return client;
}

void node_client_info_setServersocket(client_info* client, int serversocket){
    client->serversocket = serversocket;
}

int node_client_info_getServersocket(client_info* client){
    return client->serversocket;
}

void node_client_info_setSocket(client_info* client, int socket){
    client->clientsocket = socket;
}

int node_client_info_getSocket(client_info* client){
    return client->clientsocket;
}

void node_client_info_setIdentity(client_info* client, char* identity){
    if(client->identity != NULL)
        free(client->identity);
    client->identity = safe_calloc(strlen(identity) + 1);
    memcpy(client->identity, identity, strlen(identity));
}

char* node_client_info_getIdentity(client_info* client){
    return client->identity;
}


message_info* node_message_info_create(void* pduToSend, int clientsocket){
    message_info* messageInfo = safe_calloc(sizeof(struct message_info));
    messageInfo->pduToSend = pduToSend;
    messageInfo->clientsocket = clientsocket;

    return messageInfo;
}
