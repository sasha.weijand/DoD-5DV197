#include "list.h"

void init_mutex_lock (list* l);

void destroy_mutex_lock (list* l);

/** Checks if previous memoryAllocation succeded or returned NULL
  * in: pointer to memory allocated
  * out:
 **/
void check_memoryAllocation (data d);


/** Creates a new list and resutrns a pointer to it
  * in:
  * out: pointer to the created list
 **/
list* list_empty (void);


/** Sets the memHandlerFunction to the given list
  * in: pointer to list
  * out:
 **/
void setMemHandler (list* l, memFreeFunc* f);


/** Sets the compareFunction to the given list
  *in: pointer to list
  *out:
 **/
void setCompareFunc (list* l, compareFunc* f);


/** Returns the size of the given list
  * in: pointer to list
  * out: sie of the given list
 **/
int list_size (list* l);


/** Takes an integer as index and and return the position under given index .
  * If given index is larger than the size of the list NULL is returned
  * in: pointer to list, index
  * out: position corresponding to give index
 **/
list_position list_get_pos_at (list* l, int index);


/** Takes a data element and inserts it at given position in the list and returns the new node
  * in: pointer to the list, data, position to insert at
  * out: newNode
 **/
list_position list_insert (list* l, data dp, list_position p);


/** Takes  data element and inserts it in th elist at the position corresponding to the given index
  * If given index is larger than size of the list, NULL is returned
  * in: pointer to the list, data, index to insert at
  * out: newNode or NULL
 **/
list_position list_insert_at (list* l, data dp, int index);


/** Returns true or false wether the given list is empty or not
  * in: pointer to given list
  * out: true or false
 **/
bool list_isEmpty (list* l);


/** Returns the value corresponding to the given position
  * in: position
  * out: value
 **/
data list_inspect (list* l, list_position p);


/** Returns the value corresponding to the given index
  * Return NULL if given index is larger than size of the list
  * in: pointer ot given list
  * out: value or NULL
 **/
data list_inspect_at (list* l, int index);


/** Returns the first position of the list
  * in: pointer to list
  * out: the first position in the list
 **/
list_position list_first (list* l);


/** Returns the last position in the list
  * in: pointer to the list
  * out: the last position in the list
 **/
list_position list_last (list* l);


/** Takes position and returns the next position in the list
  * in: position
  * out: next position
 **/
list_position list_next (list* l, list_position p);


/** Takes a position and return the previous position in the list
  * in: position
  * out: previous position
 **/
list_position list_previous (list* l, list_position p);


/** Removes the given position in the list
  * if memHandler is set, the value is also freed
  * in: pointer to list, position
  * out: return the current position
 **/
list_position list_remove (list* l, list_position p);


/** Removes the given position to the list corresponding to the given index
  * If given index is larger than size of the list, NULL is returned
  * in: pointer to list, index
  * out: current position or NULL
 **/
list_position list_remove_at (list* l, int index);


/** Takes two positions and swaps their values
  * in: position a and postion b
  * out:
 **/
void list_swapValues (list_position a, list_position b);


/** Sorts the given list, using an insertion-sort algorithm, according to given sort terms
  * in: pointer to list
  * out:
 **/
void list_insertionSort (list* l);

void list_push (list* l, data dp);

list_position list_pop (list* l);

void list_queue (list* l, data dp);

list_position list_dequeue (list* l);

void list_clear (list* l);

/** Frees all allocated memory to given list
  * in: pointer to list
  * out:
 **/
void list_free (list* l);
