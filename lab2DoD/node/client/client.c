#include "client.h"

void client_print_InvalidArguments() {
    fprintf(stderr,
        "%s\n%s %s\n",
        "Invalid arguments:",
        "CHATCLIENT: ",
        "<IDENTITY> {ns|cs} {<NAMESERVER NAME> | <CHATSERVER NAME>} {<NAMESERVER PORT>|<CHATSERVER PORT>}");
}

void client_check_arguments(int argc, char ** argv){
    if (argc != 5){
        client_print_InvalidArguments();
        exit(EXIT_FAILURE);
    }
    if(!node_is_portNumber(argv[4])){
        client_print_InvalidArguments();
        exit(EXIT_FAILURE);
    }
}


pdu_slist* request_SLIST (host_info* ownInfo, host_info* nameServerInfo) {
    pdu_getlist* getList = pdu_getlist_create ();
    int nameserverSocket = socket_init (SENDER, TCP, nameServerInfo);
    socket_write_pdu(nameserverSocket, getList);
    pdu_getlist_free (getList);
    pdu_slist* slist = socket_read_pdu (nameserverSocket, TCP);
    close(nameserverSocket);
    return slist;
}

void flush_stdin(){
    int c;
    while((c = getchar()) != '\n' && c != EOF);
}

bool isServerSelected(char* name, char* port){
    return strcmp(name, "") != 0 || strcmp(port, "") != 0;
}

/**
 * Outputs the available servers and lets the user choose which to connect to
 */
host_info* select_chatserver (pdu_slist* slist) {
    pdu_slist_print(slist);
    printf("enter a number:\n");
    int num;
    scanf ("%d", &num);
    char* name = pdu_slist_get_server_ip_address (slist, num);
    char* port = pdu_slist_get_port (slist, num);

    if (!isServerSelected(name, port))
        select_chatserver(slist);

    flush_stdin();
    host_info* host = node_create_host_info (name, port);
    free(name);
    free(port);
    return host;
}

/**
 * Gets the connection info needed:
 * - PORT and ADRESS for name/chatserver
 */
host_info* setup_connectionInfo (char** argv, host_info* ownInfo) {
    host_info* connectInfo;
    if (strcmp (argv[2], "ns") == 0) {
        host_info* nameServerInfo = node_create_host_info(argv[3], argv[4]);
        pdu_slist* slist = request_SLIST(ownInfo, nameServerInfo);
        if (slist == NULL) {
            fprintf(stderr, "error getting slist from nameserver\n");
            host_info_free (nameServerInfo);
            return NULL;
        }
        connectInfo = select_chatserver (slist);
        pdu_slist_free(slist);
        host_info_free(nameServerInfo);
    }
    if (strcmp (argv[2], "cs") == 0) {
        connectInfo = node_create_host_info(argv[3], argv[4]);
    }

    if (connectInfo == NULL) {
        free (ownInfo);
        exit (EXIT_FAILURE);
    }

    return connectInfo;
}


void enqueue_JOIN(client_info* client){
    char* identity = node_client_info_getIdentity(client);
    int length = strlen(identity);

    pdu_join* join = pdu_join_create(length, identity);
    list* messageQueue = node_getMessageQueue(client->nodeInfo);
    list_queue_t (messageQueue, join);
}

/**
 * Connects and send JOIN to chatserver
 */
void connect_toChatserver(host_info* server, client_info* client){
    int serversocket = socket_init(SENDER, TCP, server);
    if (serversocket == 0) {
        fprintf(stderr, "Failed to connect\n");
        return;
    }
    fprintf(stderr, "Connected to chatserver: %s Port: %s\n", server->name, server->port);
    node_client_info_setServersocket(client, serversocket);
    enqueue_JOIN(client);
    online = true;
}


void client_init(){
    shouldQuit = false;
    online = false;
    rdyForInput = false;
}

/**
 * - Sets up connection info
 * - Connects to name/chatserver
 * - Starts receiver thread
 * - Starts sender thread
 * - Exits when threads are done
 */
int main(int argc, char **argv) {
    client_check_arguments(argc, argv);
    client_init();
    struct sigaction* handler = setSignalHandler(&SIG_handler);
    char* ownHostName = node_getOwnHostName();
    host_info* ownInfo = node_create_host_info(ownHostName, "");
    host_info* connectInfo = setup_connectionInfo (argv, ownInfo);
    free (ownHostName);


    node_info* node = node_create_node_info(ownInfo, connectInfo);
    client_info* client = node_create_client_info(node, argv[1]);
    connect_toChatserver(connectInfo, client);

    if (online) {

        pthread_t receiverThread, senderThread;
        if (pthread_create(&receiverThread, NULL, &client_receiver, client) < 0) {
            perror("Error creating receiver-thread");
            exit(EXIT_FAILURE);
        }
        if (pthread_create(&senderThread, NULL, &client_sender, client) < 0) {
            perror("Error creating sender-thread");
            exit(EXIT_FAILURE);
        }

        pthread_join(senderThread, NULL);
        pthread_join(receiverThread, NULL);
    }


    fprintf(stderr, "Closing client :C\n");
    close(node_client_info_getServersocket(client));
    client_info_free (client);
    free(handler);
    return EXIT_SUCCESS;
}
