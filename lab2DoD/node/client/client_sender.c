#include "client_sender.h"

bool has_outPDU(client_info* client){
    list* messageQueue = node_getMessageQueue(client->nodeInfo);
    return !list_isEmpty_t(messageQueue);
}

void send_PDU(client_info* client) {
    list* messageQueue = node_getMessageQueue(client->nodeInfo);
    void* pduToSend = list_inspect_t(messageQueue, list_first_t(messageQueue));
    socket_write_pdu (node_client_info_getServersocket(client), pduToSend);

    int op = pdu_getOP(pduToSend);
    list_dequeue_t (messageQueue);
    pdu_any_free (pduToSend);

    if( op == OP_QUIT)
        online = false;

}

void* client_sender(void * arg) {
    client_info* client = (client_info*) arg;

    while(online){
        if(has_outPDU(client))
            send_PDU(client);
    }
    sleep(1);
    return NULL;
}
