#include "client_receiver.h"


void print_participants(char* participants, int nrOfParticipants){
    int offset = 0;
    fprintf(stderr, "---------------\n");
    fprintf(stderr, "PARTICIPANTS:\n");
    fprintf(stderr, "---------------\n");
    for(int i = 0; i<nrOfParticipants; i++){
        fprintf(stderr, "%s\n", participants + offset);
        offset += strlen(participants+offset) + 1;
    }
}

void* handle_PARTICIPANTS(client_info* client, pdu_participants* pdu){
    int nrOfParticipants = pdu_participants_getNumberOfParticipants(pdu);
    char* participants = pdu_participants_getParticipants(pdu);
    print_participants(participants, nrOfParticipants);

    rdyForInput = true;
}

void* handle_PLEAVE (pdu_pleave* pdu){
    char* id = pdu_pleave_get_identity(pdu);
    printf("%s has left the chat :(\n", id);
}

void* handle_PJOIN (pdu_pjoin* pdu){
    char* id = pdu_pjoin_get_identity(pdu);
    printf("%s has joined the chat :)\n", id);
}

void* handle_QUIT (){
    printf("Connection with server dropped :C\n");
    online = false;
}

void* handle_MESS (pdu_mess* pdu){
    pdu_mess_print (pdu);
}

void* client_handle_pdu(client_info* client, void* pdu){
    int op = pdu_getOP(pdu);

    switch (op) {
        case OP_PARTICIPANTS:
            handle_PARTICIPANTS (client, pdu);
            break;
        case OP_PLEAVE:
            handle_PLEAVE (pdu);
            break;
        case OP_PJOIN:
            handle_PJOIN (pdu);
            break;
        case OP_QUIT:
            handle_QUIT ();
            break;
        case OP_MESS:
            handle_MESS (pdu);
            break;
    }
    pdu_any_free(pdu);
}

void client_enqueue_QUIT (client_info* client) {
    pdu_quit* quit = pdu_quit_create();
    list* messageQueue = node_getMessageQueue(client->nodeInfo);
    list_queue_t (messageQueue, (void*) quit);
}

void enqueue_MESS (client_info* client, char* message) {
    uint16_t length = strlen(message);
    uint8_t checksum = 0;
    uint8_t identityLength = 0;
    uint32_t timeStamp = 0;
    pdu_mess* mess = pdu_mess_create (identityLength, checksum, length, timeStamp, message, 0);
    pdu_mess_setChecksum(mess, ~pdu_mess_calc_pduSum(mess));
    list* messageQueue = node_getMessageQueue(client->nodeInfo);
    list_queue_t (messageQueue, mess);
}

bool has_userInput (){
    return socket_POLLIN(0);
}

/**
 *  Reads input from user and generates a message to send
 *  - if user inputs "quit", QUIT_pdu is enqueued
 */
char* handle_userInput(client_info *client){
    char* input = NULL;
    size_t len = 0;

    if(getline(&input, &len, stdin) != -1){
        if (strcmp (input, "quit\n") == 0)
            shouldQuit = true;
        else
            enqueue_MESS(client, input);
    }
    free(input);
}

void close_connectionERROR(){
    fprintf(stderr, "-------------------------------------------\n");
    fprintf(stderr, "SOMETHING WENT WRONG, CLOSING CONNECTION :S\n");
    fprintf(stderr, "-------------------------------------------\n");
    online = false;
}

/**
 * Checks if socket has incoming data
 * - reads incoming data and handles the read pdu
 */
void* client_handle_incomingPDUs(client_info *client){
    int serversocket = node_client_info_getServersocket(client);

    void* pdu;
    if(socket_POLL(serversocket)){
        pdu = socket_read_pdu(serversocket, TCP);
        if(pdu != NULL)
            client_handle_pdu(client, pdu);
        else
            close_connectionERROR();

    }
}

void* client_receiver(void * arg) {
    client_info* client = (client_info*) arg;
    while(online){
        if(shouldQuit){
            client_enqueue_QUIT(client);
            break;
        } else{
            client_handle_incomingPDUs(client);

            if(has_userInput () && rdyForInput)
                handle_userInput(client);
        }
    }
    return NULL;
}
