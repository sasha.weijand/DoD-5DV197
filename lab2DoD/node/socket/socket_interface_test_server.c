#include "socket_handler.h"
#include "assert.h"


bool test_server_socket_tcpAccept () {
    fprintf(stderr, "test_server_socket_tcpAccept:\n");
    char* hostName = node_getOwnHostName();
    host_info* ownInfo = node_create_host_info(hostName, "5000");
    int listenerSocket = socket_init(RECEIVER, TCP, ownInfo);
    fprintf(stderr, "    listenerSocket=%d\n", listenerSocket);
    if (listenerSocket == 0) {
        fprintf(stderr, "    listenerSocket is 0\n");
        return false;
    }
    int serverSocket = socket_tcpAccept(listenerSocket);
    if(serverSocket == 0) {
        fprintf(stderr, "    serverSocket is 0\n");
        return false;
    }
    fprintf(stderr, "    serverSocket=%d\n", serverSocket);
    close (listenerSocket);
    close (serverSocket);
    free(hostName);
    free(ownInfo);
    return true;
}

bool test_server_socket_read_pdu () {
    fprintf(stderr, "test_server_socket_read_pdu:\n");
    char* hostName = node_getOwnHostName();
    host_info* ownInfo = node_create_host_info(hostName, "5001");
    int listenerSocket = socket_init(RECEIVER, TCP, ownInfo);
    fprintf(stderr, "    listenerSocket=%d\n", listenerSocket);
    if (listenerSocket == 0) {
        fprintf(stderr, "    listenerSocket is 0\n");
        return false;
    }
    int serverSocket = socket_tcpAccept(listenerSocket);
    if(serverSocket == 0) {
        fprintf(stderr, "    serverSocket is 0\n");
        return false;
    }
    fprintf(stderr, "    serverSocket=%d\n", serverSocket);

    pdu_ack* ack = socket_read_pdu (serverSocket, TCP);
    printf("    read pdu from socket\n");
    close (listenerSocket);
    close (serverSocket);
    free(hostName);
    free(ownInfo);
    pdu_ack_free (ack);
    return true;
}

bool test_server_socket_write_pdu () {
    fprintf(stderr, "test_server_socket_write_pdu:\n");
    char* hostName = node_getOwnHostName();
    host_info* ownInfo = node_create_host_info(hostName, "5002");
    int listenerSocket = socket_init(RECEIVER, TCP, ownInfo);
    fprintf(stderr, "    listenerSocket=%d\n", listenerSocket);
    if (listenerSocket == 0) {
        fprintf(stderr, "    listenerSocket is 0\n");
        return false;
    }
    int serverSocket = socket_tcpAccept(listenerSocket);
    if(serverSocket == 0) {
        fprintf(stderr, "    serverSocket is 0\n");
        return false;
    }
    fprintf(stderr, "    serverSocket=%d\n", serverSocket);

    pdu_ack* ack = pdu_ack_create(123);
    socket_write_pdu (serverSocket, ack);
    printf("    wrote pdu to socket\n");
    close (listenerSocket);
    close (serverSocket);
    free(hostName);
    free(ownInfo);
    pdu_ack_free (ack);
    return true;
}



void testTransmitPDU (int protocolType) {
    char* hostname = node_getOwnHostName ();
    host_info* host = node_create_host_info (hostname, "1337");
    int sfd = socket_init (RECEIVER, protocolType, host);


    if (protocolType == TCP) {
        sfd = socket_tcpAccept (sfd);
    }

    while(1){
        void* pdu = socket_read_pdu (sfd, UDP);
        if(pdu != NULL){
            char* buf = pdu_serialize (pdu);

            if (assert_uint8 (*buf, OP_ACK, "send pdu_ack test")) {
                fprintf(stderr, "send pdu_ack test succeeded!\n");
            }
        }

    }

}




int main(int argc, char const *argv[]) {


    assert (test_server_socket_tcpAccept ());

    assert (test_server_socket_read_pdu());

    assert (test_server_socket_write_pdu());
    //testTransmitPDU (TCP);

    return 0;
}
