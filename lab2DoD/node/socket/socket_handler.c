#include "socket_handler.h"

void setHints(struct addrinfo* hints, int protocolType){
    memset(hints, 0, sizeof(struct addrinfo));
    hints->ai_socktype = (protocolType == UDP) ? SOCK_DGRAM : SOCK_STREAM;
    hints->ai_family = AF_INET;
    hints->ai_flags = AI_PASSIVE;
    hints->ai_protocol = 0;
    hints->ai_canonname = NULL;
    hints->ai_addr = NULL;
    hints->ai_next = NULL;
}

struct addrinfo* generateAddressInfo(host_info* host, int protocolType, struct addrinfo* result) {
    struct addrinfo hints;
    setHints(&hints, protocolType);
    int retValue = getaddrinfo (host->name, host->port, &hints, &result);
    if ( retValue != 0) {
        fprintf(stderr, "Could not find host: %s %s\n", host->name, gai_strerror(retValue));
        exit(EXIT_FAILURE);
    }
    return result;
}

int socket_create(struct addrinfo* rp){
    return socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
}

/**
 * Set the socket option to reusable
 */
void set_address_reusable (int sfd) {
    int enableOption = 1;
    if (setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &enableOption, sizeof(enableOption))==-1) {
        perror("setsockopt");
        exit(1);
    }
}

void set_timeoutoption (int sfd, int timeout) {
    struct timeval tv;
    tv.tv_sec = timeout;
    tv.tv_usec = 0;
    if (setsockopt(sfd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv))) {
        perror("setsockopt");
        exit(1);
    }
}

/**
 * Tries to connect socket.
 * - Times out after 5 seconds if no connection has been establshed
 */
bool socket_connect(int sfd, struct addrinfo* rp, host_info* receiverHost){
    uint32_t startTime = time(NULL);
    while(1){
        uint32_t now = time(NULL);
        if (now - startTime > 5) {
            return false;
        }
        if (connect(sfd, rp->ai_addr, rp->ai_addrlen) != -1){
            return true;
        }else{
            fprintf(stderr, "TRYING TO CONNECT TO: %s PORT: %s...\n", receiverHost->name, receiverHost->port);
            sleep(1);
        }
    }
}

bool socket_bind(int sfd, struct addrinfo* rp){
    if (bind(sfd, rp->ai_addr, rp->ai_addrlen) != 0){
        fprintf(stderr, "failed to bind socket: %s\n",strerror(errno) );
        return false;
    }
    return true;
}

bool socket_listen(int sfd, int listenQueue){
    if(listen(sfd, listenQueue) != 0){
        fprintf(stderr, "failed to listen socket: %s\n", strerror(errno));
        return false;
    }
    return true;

}

/**
 * Initiates a socket according to given type and protocol:
 * - gets the address
 * - sets socket options
 * - if SENDER socket- tries to connect
 * - if RECEIVER socket - tries to bind
 *
 * @param socketType
 * @param protocolType
 * @param host
 * @return socket fd
 */
int socket_init (int socketType, int protocolType, host_info* host) {
    int sfd;
    struct addrinfo* rp;
    struct addrinfo* result;
    result = generateAddressInfo(host, protocolType, result);

    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sfd = socket_create(rp);
        if (sfd == -1)
            continue;

        set_address_reusable (sfd);
        if(socketType == SENDER && protocolType == TCP)
            set_timeoutoption(sfd, 1);

        if(socketType == SENDER)
            if(socket_connect(sfd, rp, host))
                break;
            else {
                fprintf(stderr, "Connection timed out\n");
                close (sfd);
                freeaddrinfo(result);
                return 0;
            }

        if(socketType == RECEIVER)
            if(socket_bind(sfd, rp))
                break;

        close(sfd);
    }

    if (rp == NULL) {
        perror("Could not bind socket");
        exit(EXIT_FAILURE);
    }

    freeaddrinfo(result);
    return sfd;
}

/**
 * Polls the socket with POLLIN flag and RDHUP flag.
 * - if connection is close false is returned.
 * - if connection is open and connection has data to read true is returned
 * @param sfd
 * @return
 */
bool socket_POLL(int sfd){
    struct pollfd pfd[1];
    pfd[0].fd = sfd;
    pfd[0].events = POLLIN|POLLRDHUP;
    int ret = poll(pfd, 1, 0);

    if(!ret){
        return false;
    }else if(ret == -1){
        perror("poll");
        exit(EXIT_FAILURE);
    }else if(pfd[0].revents & POLLRDHUP){
        return false;
    }else if(pfd[0].revents & POLLIN)
        return true;
}

bool socket_POLLOUT(int sfd){
    struct pollfd pfd[1];
    pfd[0].fd = sfd;
    pfd[0].events = POLLOUT;
    int ret = poll(pfd, 1, 0);

    if(!ret){
        return false;
    }else if(ret == -1){
        perror("poll");
        exit(EXIT_FAILURE);
    }else if(pfd[0].revents == POLLOUT)
        return true;
}

/**
 * Polls the socket with POLLIN flag
 * - if connection has data to read return true
 * @param sfd
 * @return
 */
bool socket_POLLIN(int sfd){
    struct pollfd pfd[1];
    pfd[0].fd = sfd;
    pfd[0].events = POLLIN;
    int ret = poll(pfd, 1, 0);

    if(!ret){
        return false;
    }else if(ret == -1){
        perror("poll");
        exit(EXIT_FAILURE);
    }else if(pfd[0].revents = POLLIN)
        return true;
}

/**
 * If socket has data waiting, an accept is attempted
 * @param sfd
 * @return
 */
int socket_tcpAccept(int sfd){
    if(socket_POLLIN(sfd)){
        struct sockaddr_in clientAddress;
        memset(&clientAddress, 0, sizeof(struct sockaddr_in));
        int addressLength = sizeof(struct sockaddr_in);

        socket_listen(sfd, 255);

        int clientSocket = accept(sfd,(struct sockaddr *)&clientAddress,(socklen_t*)&addressLength);
        if(clientSocket < 0){
            perror("Receiver: Accept failed");
        }
        return clientSocket;
    }
    return 0;
}

/**
 * Reads 4 bytes from given socket and opens a pipe stream to
 * forward the data. Write end is closed when all 4 bytes are written to the stream.
 * read end is returned
 * @param sfd
 * @return
 */
int socket_read_fromUdp(int sfd){
    uint8_t buffer[4];
    if(read(sfd, buffer, 4) != 4)
       fprintf(stderr, "Could not read 4 bytes from UDP socket\n");

    int pfd[2];
    if(pipe(pfd) != 0)
        fprintf(stderr, "Could not open pipe\n");

    if(write(pfd[1], buffer, 4) != 4)
        fprintf(stderr, "Could not write 4 bytes to socket\n");

    close(pfd[1]);

    return pfd[0];
}

/**
 * Reads a PDU with deserialize, returns the read PDU, NULL if something went wrong
 * @param sfd
 * @param protocolType
 * @return
 */
void* socket_read_pdu(int sfd, int protocolType) {

    if(protocolType == UDP)
        sfd = socket_read_fromUdp(sfd);

    void* pdu = pdu_deserialize(sfd);

    if(protocolType == UDP)
        close(sfd);

    return pdu;

}

/**
 * Writes PDU to socket
 * @param sfd
 * @param pduToWrite
 */
void socket_write_pdu(int sfd, void* pduToWrite){
    uint8_t* buffer = pdu_serialize(pduToWrite);
    int size = pdu_getSize(pduToWrite);
    int nwrite;
    if(socket_POLLOUT(sfd))
        if((nwrite = write(sfd, buffer, size )) != size){
            fprintf(stderr, "Could not write whole pdu to socket: %d Could only write %d of %d bytes\n", sfd, nwrite, size);
        }

    free(buffer);

}
