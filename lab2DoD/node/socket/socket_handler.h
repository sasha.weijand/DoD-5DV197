#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE 700
#endif
#ifndef _POSIX_SOURCE
#define _POSIX_SOURCE
#endif

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#define BUFFERSIZE 100
#define UDP 0
#define TCP 1
#define RECEIVER 2
#define SENDER 3

#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netdb.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <math.h>
#include <sys/time.h>

#include "../node.h"
#include "../list/thread_list_lib.h"
#include "../../pdu/pdu.h"

#ifndef _SOCKETHANDLER_H
#define _SOCKETHANDLER_H


/*  Accepts tcpConnection
 *
 *  @return: sfd to receiving socket
 */
int socket_tcpAccept(int sfd);

/*  Creates and initiates a socket according to given type:
 *   - If socket type is SENDER: socket is also connected
 *   - If socket type is RECEIVER: socket is also bound to local port
 *
 *  @return: sfd
 */
int socket_init (int socketType, int protocolType, host_info* host);

void* socket_read_pdu(int sfd, int protocolType);

bool socket_POLLIN(int sfd);

bool socket_POLLOUT(int sfd);

bool socket_POLL(int sfd);

void socket_write_pdu(int sfd, void* pduToWrite);
#endif
