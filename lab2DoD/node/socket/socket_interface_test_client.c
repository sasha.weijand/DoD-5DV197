#include "socket_handler.h"
#include "assert.h"


bool test_client_socket_tcpAccept () {
    fprintf(stderr, "test_client_socket_tcpAccept:\n");
    char* hostName = node_getOwnHostName();
    host_info* ownInfo = node_create_host_info(hostName, "5000");
    int clientSocket = socket_init(SENDER, TCP, ownInfo);
    if(clientSocket == 0) {
        fprintf(stderr, "    clientSocket is 0\n");
        return false;
    }
    fprintf(stderr, "    clientSocket=%d\n", clientSocket);
    close (clientSocket);
    free(hostName);
    free(ownInfo);
    return true;
}

bool test_client_socket_write_pdu () {
    fprintf(stderr, "test_client_socket_write_pdu:\n");
    char* hostName = node_getOwnHostName();
    host_info* ownInfo = node_create_host_info(hostName, "5001");
    int clientSocket = socket_init(SENDER, TCP, ownInfo);
    if(clientSocket == 0) {
        fprintf(stderr, "    clientSocket is 0\n");
        return false;
    }
    fprintf(stderr, "    clientSocket=%d\n", clientSocket);
    pdu_ack* ack = pdu_ack_create(123);
    socket_write_pdu (clientSocket, ack);
    printf("    wrote pdu to socket\n");
    close (clientSocket);
    free(hostName);
    free(ownInfo);
    pdu_ack_free (ack);
    return true;
}

bool test_client_socket_read_pdu () {
    fprintf(stderr, "test_client_socket_read_pdu:\n");
    char* hostName = node_getOwnHostName();
    host_info* ownInfo = node_create_host_info(hostName, "5002");
    int clientSocket = socket_init(SENDER, TCP, ownInfo);
    if(clientSocket == 0) {
        fprintf(stderr, "    clientSocket is 0\n");
        return false;
    }
    fprintf(stderr, "    clientSocket=%d\n", clientSocket);
    pdu_ack* ack = socket_read_pdu (clientSocket, TCP);
    printf("    read pdu from socket\n");
    close (clientSocket);
    free(hostName);
    free(ownInfo);
    pdu_ack_free (ack);
    return true;
}

void testTransmitPDU_client (int protocolType) {

    host_info* host = node_create_host_info (node_getOwnHostName (), "1337");
    int clientSfd = socket_init (SENDER, protocolType, host);

    fprintf(stderr, "sfd = %d\n", clientSfd );


    while (1) {
        pdu_ack* ack = pdu_ack_create (123);
        socket_write_pdu (clientSfd, ack);
        sleep(3);
    }

}


int main(int argc, char const *argv[]) {


    assert (test_client_socket_tcpAccept ());

    assert (test_client_socket_write_pdu());

    assert (test_client_socket_read_pdu());
    //testTransmitPDU_client (TCP);

    return 0;
}
