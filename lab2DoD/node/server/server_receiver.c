#include "server_receiver.h"

/**
 *  Checks the heartbeat response from name server and reacts accordingly.
 */
void check_heartbeat(server_info* server){
    int nameserverSocket = node_server_info_getNameserverSocket(server);

    void* pdu;
    if(socket_POLLIN(nameserverSocket)){
        pdu = socket_read_pdu(nameserverSocket, UDP);

        if(pdu != NULL){
            int op = pdu_getOP(pdu);
            if( op == OP_ACK){
                if(!registered){
                    fprintf(stderr, "SERVER IS NOW CONNECTED TO NAMESERVER\n");
                    node_server_info_setID(server, pdu_ack_getID((pdu_ack*)pdu));
                    registered = true;
                }
            }
            if(op == OP_NOTREG){
                fprintf(stderr, "SERVER LOST CONNECTION WITH NAMESERVER: RECONNECTING\n");
                registered = false;

            }
            pdu_any_free(pdu);
        }
    }
}

void remove_client(server_info* server, client_info* clientToRemove){
    list* clients = node_server_info_getClients(server);
    int nrOfClients = node_server_info_getNumberOfClients(server);


    for(int i = 0; i<nrOfClients; i++){
        client_info* client = (client_info*)list_inspect_at_t(clients, i);
        int clientsocket = node_client_info_getSocket(client);
        if (clientsocket == node_client_info_getSocket(clientToRemove)){
            list_remove_at_t(clients, i);
            close(node_client_info_getSocket(client));
            fprintf(stderr, "Client Removed");
            fprintf(stderr, "Number of clients: %d\n", node_server_info_getNumberOfClients(server));
            break;
        }
    }
}

void* generate_PLEAVE(server_info* server, client_info* client){
    uint8_t* identity = client->identity;
    uint8_t length = strlen(identity);
    uint32_t timeStamp = time(NULL);

    return pdu_pleave_create(length, identity, timeStamp);
}

void* generate_PJOIN(server_info* server, client_info* client){
    uint8_t* identity = client->identity;
    uint8_t length = strlen(identity);
    uint32_t timeStamp = time(NULL);

    return pdu_pjoin_create(length, identity, timeStamp);
}

int calculate_participantsLength(server_info* server ){
    list* clients = node_server_info_getClients(server);
    int nrOfClients = node_server_info_getNumberOfClients(server);

    int length = 0;
    for(int i = 0; i<nrOfClients; i++){
        client_info* client = list_inspect_at_t(clients, i);
        char* identity = client->identity;
        if(identity != NULL)
            length = length + strlen(identity) + 1;
    }
    return length;
}

/**
 * Makes a list of chat participants.
 */
void* generate_PARTICIPANTS(server_info* server){
    list* clients = node_server_info_getClients(server);
    int nrOfClients = node_server_info_getNumberOfClients(server);
    int length = calculate_participantsLength(server);
    int nrOfJoinedClients = 0;

    uint8_t buffer[length];
    memset(buffer, 0, length);
    int offset = 0;
    for(int i = 0; i<nrOfClients; i++){
        client_info* client = list_inspect_at_t(clients, i);
        char* identity = client->identity;
        if(identity != NULL){
            memcpy(buffer + offset, identity, strlen(identity));
            offset = offset + strlen(identity) + 1;
            nrOfJoinedClients ++;
        }
    }

    return pdu_participants_create(nrOfJoinedClients, length, buffer);
}

void enqueue_messageInfo(server_info* server, message_info* messageInfo){
    list* messageQueue = node_getMessageQueue(server->nodeInfo);
    list_queue_t(messageQueue, messageInfo);
}

void enqueue_PLEAVE(server_info* server, client_info* client){
    int clientsocket = node_client_info_getSocket(client);

    void* pleave = generate_PLEAVE(server, client);
    enqueue_messageInfo(server, node_message_info_create(pleave, clientsocket));
}

void enqueue_PARTICIPANTS(server_info* server, client_info* client){
    int clientsocket = node_client_info_getSocket(client);

    void* participants = generate_PARTICIPANTS(server);
    enqueue_messageInfo(server, node_message_info_create(participants, clientsocket));
}

void enqueue_PJOIN(server_info* server, client_info* client){
    int clientsocket = node_client_info_getSocket(client);

    void* pjoin = generate_PJOIN(server, client);
    enqueue_messageInfo(server, node_message_info_create(pjoin, clientsocket));
}

void* generate_QUIT(){
    return pdu_quit_create();
}

void enqueue_QUIT(server_info* server){
    void* quit = generate_QUIT();
    enqueue_messageInfo(server, node_message_info_create(quit, 0));
}

void update_MESS(client_info* client, void* pdu){
    pdu_mess* mess = pdu;
    pdu_mess_setIdentity(mess, client->identity, strlen(client->identity));
    pdu_mess_setTimestamp(mess, time(NULL));
    pdu_mess_setChecksum(mess, 0);
    pdu_mess_setChecksum(mess, ~pdu_mess_calc_pduSum(mess));
    pdu_mess_updateSize(mess);

}

void enqueue_MESS(server_info* server, client_info* client, void* pdu){
    int clientsocket = node_client_info_getSocket(client);
    enqueue_messageInfo(server, node_message_info_create(pdu, clientsocket));
}


void* generate_SERVER_MESS(char* message){
    uint32_t timeStamp = time(NULL);
    pdu_mess* mess = pdu_mess_create(0, 0, strlen(message), timeStamp, message, 0);
    pdu_mess_setChecksum(mess, ~pdu_mess_calc_pduSum(mess));

    return mess;
}

void set_clientIdentity(client_info* client, pdu_join* pdu){
    node_client_info_setIdentity(client, pdu_join_getIdentity(pdu));
    fprintf(stderr, "Client Joined: %s\n", pdu_join_getIdentity(pdu));
}

void handle_pdu(server_info* server, client_info* client, void* pdu){

    int op = pdu_getOP(pdu);

    if(op == OP_QUIT){
        remove_client(server, client);
        enqueue_PLEAVE(server, client);
    }

    if(op == OP_JOIN){
        set_clientIdentity(client, (pdu_join*)pdu);
        enqueue_PJOIN(server, client);
        enqueue_PARTICIPANTS(server, client);
    }

    if(op == OP_MESS){
        fprintf(stderr, "MESS received\n");
        update_MESS(client, pdu);
        enqueue_MESS(server, client, pdu);
    }

    if(op != OP_MESS)
        pdu_any_free(pdu);
}

void kick_client(server_info* server, client_info* client){
    void* mess = generate_SERVER_MESS("CLIENT HAS BEEN KICKED FOR SENDING INVALID DATA\n");
    enqueue_MESS(server, client, mess);
    remove_client(server, client);
    fprintf(stderr, "CLIENT KICKED\n");
}


/**
 *  For every client on the list, check that client's socket for incoming PDU:s.
 *  If there is any PDU, read and handle it according to content.
 */
void check_incomingPDUs(server_info* server){

    pthread_mutex_lock(&clientsLock);
    list* clients = node_server_info_getClients(server);
    int nrOfClients = node_server_info_getNumberOfClients(server);

    for(int i = 0; i<nrOfClients; i++){
        client_info* client = (client_info*) list_inspect_at_t(clients, i);
        if(client != NULL){
            int clientsocket = client->clientsocket;
            void* pdu;
            if(socket_POLL(clientsocket)){
                pdu = socket_read_pdu(clientsocket, TCP);
                if(pdu != NULL){
                    handle_pdu(server, client, pdu);
                }else{
                    kick_client(server, client);
                }
            }
        }
    }
    pthread_mutex_unlock(&clientsLock);
}

void add_client(server_info* server, int clientSocket){
    client_info* client = node_create_client_info(0,"");
    node_client_info_setSocket(client, clientSocket);
    list* clients = node_server_info_getClients(server);
    list_queue_t(clients, client);
}


/**
 *  Checks the tcp listening socket for new requests from clients, and adds them to the list of clients.
 */
void check_newClients(server_info* server, int listenerSocket){

    int clientSocket = socket_tcpAccept(listenerSocket);
    if(clientSocket != 0){
        pthread_mutex_lock(&clientsLock);
        add_client(server, clientSocket);
        fprintf(stderr, "Client connected FD: %d\n", clientSocket);
        fprintf(stderr, "Number of clients: %d\n", node_server_info_getNumberOfClients(server));
        pthread_mutex_unlock(&clientsLock);
    }

}


/**
 * While the server is still online and registered at the nameserver,
 * check for new clients and incoming PDU:s.
 */
void* server_receiver(void * arg) {
    server_info* server = (server_info*) arg;
    int listenerSocket = socket_init(RECEIVER, TCP, server->nodeInfo->ownInfo);
    while(online){
        if(shouldQuit)
            enqueue_QUIT(server);
        else{
            check_heartbeat(server);
            if(registered){
                check_newClients(server, listenerSocket);
                check_incomingPDUs(server);
            }
        }
    }
    return NULL;
}
