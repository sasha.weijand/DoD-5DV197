#include "server.h"

void server_print_InvalidArguments() {
    fprintf(stderr,
        "%s\n%s %s\n",
        "Invalid arguments:",
        "CHATSERVER: ",
        "<LOCAL PORT> <SERVER NAME> <NAME SERVER HOST> <NAME SERVER PORT>");
}


void server_check_arguments(int argc, char ** argv){

    if (argc != 5){
        server_print_InvalidArguments("SERVER");
        exit(EXIT_FAILURE);
    }

    if(!node_is_portNumber(argv[1])){
        server_print_InvalidArguments("SERVER");
        exit(EXIT_FAILURE);
    }

    if(!node_is_portNumber(argv[4])){
        server_print_InvalidArguments("SERVER");
        exit(EXIT_FAILURE);
    }

}


/**
 * Creates a server_info from the arguments
 */
server_info* server_create_serverParameters(char** argv){
    char* hostname = node_getOwnHostName();
    host_info* ownInfo = node_create_host_info(hostname, argv[1]);
    host_info* connectInfo = node_create_host_info(argv[3], argv[4]);

    int nameserverSocket = socket_init(SENDER, UDP, connectInfo);
    free(hostname);
    return node_create_server_info(node_create_node_info(ownInfo, connectInfo), argv[2], nameserverSocket);
}



void server_init(){
    online = true;
    registered = false;
    node_init_clients_lock();
}


/**
 * - Creates parameters for receiver and sender
 * - Starts a receiver thread
 * - Starts a sender thread
 */
int main(int argc, char **argv) {
    server_check_arguments(argc, argv);
    server_init();
    struct sigaction* handler = setSignalHandler(&SIG_handler);
    server_info* serverParams = server_create_serverParameters(argv);

    pthread_t receiverThread, senderThread;
    if (pthread_create(&receiverThread, NULL, &server_receiver, serverParams) < 0) {
        perror("Error creating listener-thread");
        exit(EXIT_FAILURE);
    }
    if (pthread_create(&senderThread, NULL, &server_sender, serverParams) < 0) {
        perror("Error creating sender-thread");
        exit(EXIT_FAILURE);
    }

    pthread_join(senderThread, NULL);
    pthread_join(receiverThread, NULL);
    node_destroy_clients_lock();
    free(handler);
    server_info_free(serverParams);
    fprintf(stderr, "Closing server\n");
    return EXIT_SUCCESS;
}
