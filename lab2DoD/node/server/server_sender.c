#include "server_sender.h"

void send_REG(server_info* server){
    int nameserverSocket = node_server_info_getNameserverSocket(server);
    char* servername = node_server_info_getServername(server);
    char* port = node_server_info_getPort(server);

    pdu_reg* pdu = pdu_reg_create(strlen(servername), servername, atoi(port));
    socket_write_pdu(nameserverSocket, pdu);
    pdu_reg_free(pdu);
}

void send_ALIVE(server_info* server, struct timeval* time_aliveSent){
    int nameserverSocket = node_server_info_getNameserverSocket(server);
    int nrOfClients = node_server_info_getNumberOfClients(server);
    int ID = node_server_info_getID(server);

    pdu_alive* pdu = pdu_alive_create(nrOfClients, ID);
    socket_write_pdu(nameserverSocket, pdu);
    gettimeofday(time_aliveSent, NULL);
    pdu_alive_free(pdu);
}

bool should_send_ALIVE(struct timeval time_aliveSent){
    struct timeval current;
    gettimeofday(&current, NULL);
    uint32_t timeWaited = current.tv_sec - time_aliveSent.tv_sec;

    return timeWaited > 8 || time_aliveSent.tv_sec == 0;
}

bool has_message(server_info* server){
    list* messageQueue = node_getMessageQueue(server->nodeInfo);
    return !list_isEmpty_t(messageQueue);
}

/**
 * Sends a PDU to all clients
 */
void broadcast_PDU(server_info* server, void* pduToSend){
    pthread_mutex_lock(&clientsLock);
    list* clients = node_server_info_getClients(server);
    int nrOfClients = node_server_info_getNumberOfClients(server);

    int op = pdu_getOP(pduToSend);

    for(int i = 0; i < nrOfClients; i++){
        client_info* client = (client_info*)list_inspect_at_t(clients, i);
        int sfd = client->clientsocket;
        socket_write_pdu(sfd, pduToSend);
    }
    pthread_mutex_unlock(&clientsLock);
}

/**
 * Sends a PDU
 */
void send_message(server_info* server){
    list* messageQueue = node_getMessageQueue(server->nodeInfo);
    message_info* info = list_inspect_t(messageQueue, list_first_t(messageQueue));
    void* pduToSend = info->pduToSend;
    int clientsocket = info->clientsocket;

    int op = pdu_getOP(pduToSend);

    if(op == OP_MESS || op == OP_PJOIN || op == OP_PLEAVE || op == OP_QUIT)
        broadcast_PDU(server, pduToSend);
    else
        socket_write_pdu(clientsocket, pduToSend);

    message_info_free(info);
    list_dequeue_t(messageQueue);
    if(op == OP_QUIT)
        online = false;
}

void* server_sender(void * arg) {
    server_info* server = (server_info*) arg;

    struct timeval time_aliveSent;
    time_aliveSent.tv_sec = 0;

    while(online){
        if(registered){
            if (should_send_ALIVE(time_aliveSent))
                send_ALIVE( server, &time_aliveSent);
        }else{
            send_REG(server);
        }

        if(has_message(server))
           send_message(server);

    }
    sleep(1);
    return NULL;
}
