#!/bin/bash

#input: name of host computer, nr of clients to be started

SERVER=$1
PORT=$2
NR_PARTICIPANTS=$3
#sleep 8
#../chatserver $PORT bananrepubliken itchy.cs.umu.se 1337 &
#sleep 1

for ((i=0; i< $NR_PARTICIPANTS; ++i)); do

    ../chatclient apan$i cs $SERVER $PORT < copyPaste &
done

#sleep 20
#pkill chatserver

#maybe do manually after run (depending on what's in 'meddelande'):
#pkill chatclient -9
